# Strapi application

## Using Docker

1. Create `config/database.js` as follows:

```js
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'postgres'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'strapi'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', 'strapi'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
```

2. Pull the latest images

```bash
docker-compose pull
```

3. Run the stack

```bash
docker-compose up -d
```

Reference: https://strapi.io/documentation/v3.x/installation/docker.html

## Manually

### Requirements

1. NodeJS 12
2. `npm -g install yarn`

### How to

1. `yarn`
2. Create database `influencer_nonprofit` on your PostgreSQL instance, i.e.:

        psql -hlocalhost -Upostgres postgres
        CREATE DATABASE influencer_nonprofit ENCODING 'UTF8';

3. Create `config/database.js` from `config/database.dev.js` (template) using your own local PostgreSQL configuration (i.e. password).

```
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: 'localhost',
        database: 'influencer_nonprofit',
        username: 'postgres',
        password: '*********',
        // filename: env('DATABASE_FILENAME', '.tmp/data.db'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
```

### To run

```
yarn develop
```

## To inspect the database:

Use your favorite PostgreSQL database admin, e.g. https://www.pgadmin.org/ .

In order to reconstruct the project from Strapi backend, the following PostgreSQL dumps are needed:

* Schema: `cloutvocate-schema.sql`
* Data: `cloutvocate-data.sql`
