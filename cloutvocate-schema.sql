--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

-- Started on 2020-07-22 20:05:31 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13920)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 4264 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 261 (class 1259 OID 25503)
-- Name: campaigns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.campaigns (
    id integer NOT NULL,
    "youtubeVideoLink" character varying(255),
    proposal integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.campaigns OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 25501)
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.campaigns_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campaigns_id_seq OWNER TO postgres;

--
-- TOC entry 4265 (class 0 OID 0)
-- Dependencies: 260
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.campaigns_id_seq OWNED BY public.campaigns.id;


--
-- TOC entry 211 (class 1259 OID 24644)
-- Name: core_store; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.core_store (
    id integer NOT NULL,
    key character varying(255),
    value text,
    type character varying(255),
    environment character varying(255),
    tag character varying(255)
);


ALTER TABLE public.core_store OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24611)
-- Name: core_store_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.core_store_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_store_id_seq OWNER TO postgres;

--
-- TOC entry 4266 (class 0 OID 0)
-- Dependencies: 204
-- Name: core_store_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.core_store_id_seq OWNED BY public.core_store.id;


--
-- TOC entry 207 (class 1259 OID 24621)
-- Name: influencers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.influencers (
    id integer NOT NULL,
    name character varying(255),
    "instagramUsername" character varying(255),
    "youtubeChannelUrl" character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    "firstName" character varying(255),
    "lastName" character varying(255),
    "interestedInDonating" boolean,
    range_of_compensation integer,
    "user" integer,
    "googleAuthToken" text,
    "amazonAuthToken" text,
    "googlePhotoUrl" character varying(255)
);


ALTER TABLE public.influencers OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24603)
-- Name: influencers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.influencers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.influencers_id_seq OWNER TO postgres;

--
-- TOC entry 4267 (class 0 OID 0)
-- Dependencies: 200
-- Name: influencers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.influencers_id_seq OWNED BY public.influencers.id;


--
-- TOC entry 275 (class 1259 OID 26228)
-- Name: influencers_type_of_influencers__type_of_influencers_influencer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.influencers_type_of_influencers__type_of_influencers_influencer (
    id integer NOT NULL,
    influencer_id integer,
    "type-of-influencer_id" integer
);


ALTER TABLE public.influencers_type_of_influencers__type_of_influencers_influencer OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 26226)
-- Name: influencers_type_of_influencers__type_of_influencers_inf_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.influencers_type_of_influencers__type_of_influencers_inf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.influencers_type_of_influencers__type_of_influencers_inf_id_seq OWNER TO postgres;

--
-- TOC entry 4268 (class 0 OID 0)
-- Dependencies: 274
-- Name: influencers_type_of_influencers__type_of_influencers_inf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.influencers_type_of_influencers__type_of_influencers_inf_id_seq OWNED BY public.influencers_type_of_influencers__type_of_influencers_influencer.id;


--
-- TOC entry 281 (class 1259 OID 26252)
-- Name: influencers_type_of_non_profit_organisations__type_of_non_profi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.influencers_type_of_non_profit_organisations__type_of_non_profi (
    id integer NOT NULL,
    influencer_id integer,
    "type-of-non-profit-organisation_id" integer
);


ALTER TABLE public.influencers_type_of_non_profit_organisations__type_of_non_profi OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 26250)
-- Name: influencers_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.influencers_type_of_non_profit_organisations__type_of_no_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.influencers_type_of_non_profit_organisations__type_of_no_id_seq OWNER TO postgres;

--
-- TOC entry 4269 (class 0 OID 0)
-- Dependencies: 280
-- Name: influencers_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.influencers_type_of_non_profit_organisations__type_of_no_id_seq OWNED BY public.influencers_type_of_non_profit_organisations__type_of_non_profi.id;


--
-- TOC entry 251 (class 1259 OID 25391)
-- Name: non_profits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.non_profits (
    id integer NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    organisation character varying(255),
    "user" integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.non_profits OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 25389)
-- Name: non_profits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.non_profits_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.non_profits_id_seq OWNER TO postgres;

--
-- TOC entry 4270 (class 0 OID 0)
-- Dependencies: 250
-- Name: non_profits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.non_profits_id_seq OWNED BY public.non_profits.id;


--
-- TOC entry 277 (class 1259 OID 26236)
-- Name: non_profits_type_of_influencers__type_of_influencers_non_profit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.non_profits_type_of_influencers__type_of_influencers_non_profit (
    id integer NOT NULL,
    "non-profit_id" integer,
    "type-of-influencer_id" integer
);


ALTER TABLE public.non_profits_type_of_influencers__type_of_influencers_non_profit OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 26234)
-- Name: non_profits_type_of_influencers__type_of_influencers_non_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.non_profits_type_of_influencers__type_of_influencers_non_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.non_profits_type_of_influencers__type_of_influencers_non_id_seq OWNER TO postgres;

--
-- TOC entry 4271 (class 0 OID 0)
-- Dependencies: 276
-- Name: non_profits_type_of_influencers__type_of_influencers_non_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.non_profits_type_of_influencers__type_of_influencers_non_id_seq OWNED BY public.non_profits_type_of_influencers__type_of_influencers_non_profit.id;


--
-- TOC entry 283 (class 1259 OID 26260)
-- Name: non_profits_type_of_non_profit_organisations__type_of_non_profi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.non_profits_type_of_non_profit_organisations__type_of_non_profi (
    id integer NOT NULL,
    "non-profit_id" integer,
    "type-of-non-profit-organisation_id" integer
);


ALTER TABLE public.non_profits_type_of_non_profit_organisations__type_of_non_profi OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 26258)
-- Name: non_profits_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.non_profits_type_of_non_profit_organisations__type_of_no_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.non_profits_type_of_non_profit_organisations__type_of_no_id_seq OWNER TO postgres;

--
-- TOC entry 4272 (class 0 OID 0)
-- Dependencies: 282
-- Name: non_profits_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.non_profits_type_of_non_profit_organisations__type_of_no_id_seq OWNED BY public.non_profits_type_of_non_profit_organisations__type_of_non_profi.id;


--
-- TOC entry 230 (class 1259 OID 24793)
-- Name: organizationlist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organizationlist (
    charitynavigatorurl text,
    charityname text,
    ein integer,
    irsclassification text,
    usstate text,
    id integer NOT NULL
);


ALTER TABLE public.organizationlist OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 24872)
-- Name: organizationlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.organizationlist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizationlist_id_seq OWNER TO postgres;

--
-- TOC entry 4273 (class 0 OID 0)
-- Dependencies: 241
-- Name: organizationlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.organizationlist_id_seq OWNED BY public.organizationlist.id;


--
-- TOC entry 210 (class 1259 OID 24641)
-- Name: organizations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organizations (
    id integer NOT NULL,
    name character varying(255),
    email character varying(255),
    "websiteUrl" character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.organizations OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 24609)
-- Name: organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.organizations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizations_id_seq OWNER TO postgres;

--
-- TOC entry 4274 (class 0 OID 0)
-- Dependencies: 203
-- Name: organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.organizations_id_seq OWNED BY public.organizations.id;


--
-- TOC entry 263 (class 1259 OID 25513)
-- Name: payment_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_statuses (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.payment_statuses OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 25511)
-- Name: payment_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_statuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_statuses_id_seq OWNER TO postgres;

--
-- TOC entry 4275 (class 0 OID 0)
-- Dependencies: 262
-- Name: payment_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_statuses_id_seq OWNED BY public.payment_statuses.id;


--
-- TOC entry 267 (class 1259 OID 25536)
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    id integer NOT NULL,
    "from" integer,
    "to" integer,
    amount double precision,
    payment_status integer,
    proposal integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 25534)
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payments_id_seq OWNER TO postgres;

--
-- TOC entry 4276 (class 0 OID 0)
-- Dependencies: 266
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- TOC entry 234 (class 1259 OID 24816)
-- Name: proposal_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposal_statuses (
    id integer NOT NULL,
    status character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    proposal integer,
    name character varying(255),
    color character varying(255)
);


ALTER TABLE public.proposal_statuses OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 24814)
-- Name: proposal_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proposal_statuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposal_statuses_id_seq OWNER TO postgres;

--
-- TOC entry 4277 (class 0 OID 0)
-- Dependencies: 233
-- Name: proposal_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proposal_statuses_id_seq OWNED BY public.proposal_statuses.id;


--
-- TOC entry 253 (class 1259 OID 25404)
-- Name: proposal_visibilities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposal_visibilities (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.proposal_visibilities OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 25402)
-- Name: proposal_visibilities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proposal_visibilities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposal_visibilities_id_seq OWNER TO postgres;

--
-- TOC entry 4278 (class 0 OID 0)
-- Dependencies: 252
-- Name: proposal_visibilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proposal_visibilities_id_seq OWNED BY public.proposal_visibilities.id;


--
-- TOC entry 240 (class 1259 OID 24849)
-- Name: proposals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposals (
    id integer NOT NULL,
    questionnaire_non_profit integer,
    questionnaire_influencer integer,
    proposal_status integer,
    sponsor_donation integer,
    "campaignName" character varying(255),
    type_of_non_profit_organisation integer,
    "campaignDescription" text,
    "campaignStartDate" date,
    "CampaignEndDate" date,
    "ProposalApprovalDate" date,
    target_campaign integer,
    "howShouldItLook" text,
    calltoaction text,
    "allowSponsor" boolean,
    "promptingCampaignOnPlatform" boolean,
    "sendOnlyToGivingBack" boolean,
    "offerTaxReceipt" boolean,
    "extraInformation" text,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    non_profit integer,
    "campaignEndDate" date,
    "proposalApprovalDate" date,
    "howShouldItLookNonProfit" text,
    "callToActionNonProfit" text,
    "sponsorshipIntegration" boolean,
    "nonprofitOrganizationPromotion" boolean,
    "sendOnlyToInfluencers" boolean,
    "taxReceipt" boolean,
    "anyThingElseNonProfit" text,
    "statusInfluencer" integer,
    "percentRevenueInfluencer" integer,
    "anythingElseInfluencer" text,
    influencer integer,
    "statusSponsor" integer,
    "budgetSponsor" integer,
    "howShouldItLookSponsor" text,
    "callToActionSponsor" text,
    "anyThingElseSponsor" text,
    "statusInfluencerWithSponsor" integer,
    "statusNonProfitWithSponsor" integer,
    "isACampaign" boolean,
    "promotingCampaignSponsor" boolean,
    sponsor integer,
    campaign integer,
    "paymentInfluencer" integer,
    "paymentStoI" integer,
    "paymentStoNP" integer,
    "hasCampaignStarted" boolean,
    "hasCampaignEnded" boolean
);


ALTER TABLE public.proposals OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 24847)
-- Name: proposals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proposals_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposals_id_seq OWNER TO postgres;

--
-- TOC entry 4279 (class 0 OID 0)
-- Dependencies: 239
-- Name: proposals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proposals_id_seq OWNED BY public.proposals.id;


--
-- TOC entry 273 (class 1259 OID 26220)
-- Name: proposals_proposal_visibilities__proposal_visibilities_proposal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposals_proposal_visibilities__proposal_visibilities_proposal (
    id integer NOT NULL,
    proposal_id integer,
    "proposal-visibility_id" integer
);


ALTER TABLE public.proposals_proposal_visibilities__proposal_visibilities_proposal OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 26218)
-- Name: proposals_proposal_visibilities__proposal_visibilities_p_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proposals_proposal_visibilities__proposal_visibilities_p_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposals_proposal_visibilities__proposal_visibilities_p_id_seq OWNER TO postgres;

--
-- TOC entry 4280 (class 0 OID 0)
-- Dependencies: 272
-- Name: proposals_proposal_visibilities__proposal_visibilities_p_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proposals_proposal_visibilities__proposal_visibilities_p_id_seq OWNED BY public.proposals_proposal_visibilities__proposal_visibilities_proposal.id;


--
-- TOC entry 265 (class 1259 OID 25523)
-- Name: public_donations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.public_donations (
    id integer NOT NULL,
    payment integer,
    "firstName" character varying(255),
    "lastName" character varying(255),
    "emailAddress" character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.public_donations OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 25521)
-- Name: public_donations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.public_donations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.public_donations_id_seq OWNER TO postgres;

--
-- TOC entry 4281 (class 0 OID 0)
-- Dependencies: 264
-- Name: public_donations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.public_donations_id_seq OWNED BY public.public_donations.id;


--
-- TOC entry 227 (class 1259 OID 24770)
-- Name: questionnaire_corporates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_corporates (
    id integer NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    name character varying(255),
    "user" integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    sponsor_donation integer
);


ALTER TABLE public.questionnaire_corporates OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 24768)
-- Name: questionnaire_corporates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_corporates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_corporates_id_seq OWNER TO postgres;

--
-- TOC entry 4282 (class 0 OID 0)
-- Dependencies: 226
-- Name: questionnaire_corporates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_corporates_id_seq OWNED BY public.questionnaire_corporates.id;


--
-- TOC entry 243 (class 1259 OID 25357)
-- Name: questionnaire_corporates_type_of_contents__type_of_contents_que; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_corporates_type_of_contents__type_of_contents_que (
    id integer NOT NULL,
    "questionnaire-corporate_id" integer,
    "type-of-content_id" integer
);


ALTER TABLE public.questionnaire_corporates_type_of_contents__type_of_contents_que OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 25355)
-- Name: questionnaire_corporates_type_of_contents__type_of_conte_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_corporates_type_of_contents__type_of_conte_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_corporates_type_of_contents__type_of_conte_id_seq OWNER TO postgres;

--
-- TOC entry 4283 (class 0 OID 0)
-- Dependencies: 242
-- Name: questionnaire_corporates_type_of_contents__type_of_conte_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_corporates_type_of_contents__type_of_conte_id_seq OWNED BY public.questionnaire_corporates_type_of_contents__type_of_contents_que.id;


--
-- TOC entry 247 (class 1259 OID 25373)
-- Name: questionnaire_corporates_type_of_non_profit_organisations__type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_corporates_type_of_non_profit_organisations__type (
    id integer NOT NULL,
    "questionnaire-corporate_id" integer,
    "type-of-non-profit-organisation_id" integer
);


ALTER TABLE public.questionnaire_corporates_type_of_non_profit_organisations__type OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 25371)
-- Name: questionnaire_corporates_type_of_non_profit_organisation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_corporates_type_of_non_profit_organisation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_corporates_type_of_non_profit_organisation_id_seq OWNER TO postgres;

--
-- TOC entry 4284 (class 0 OID 0)
-- Dependencies: 246
-- Name: questionnaire_corporates_type_of_non_profit_organisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_corporates_type_of_non_profit_organisation_id_seq OWNED BY public.questionnaire_corporates_type_of_non_profit_organisations__type.id;


--
-- TOC entry 225 (class 1259 OID 24757)
-- Name: questionnaire_influencers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_influencers (
    id integer NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    "interestedInDonating" boolean,
    range_of_compensation integer,
    "user" integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    social_auth_token integer
);


ALTER TABLE public.questionnaire_influencers OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 24755)
-- Name: questionnaire_influencers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_influencers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_influencers_id_seq OWNER TO postgres;

--
-- TOC entry 4285 (class 0 OID 0)
-- Dependencies: 224
-- Name: questionnaire_influencers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_influencers_id_seq OWNED BY public.questionnaire_influencers.id;


--
-- TOC entry 245 (class 1259 OID 25365)
-- Name: questionnaire_influencers_type_of_contents__type_of_contents_qu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_influencers_type_of_contents__type_of_contents_qu (
    id integer NOT NULL,
    "questionnaire-influencer_id" integer,
    "type-of-content_id" integer
);


ALTER TABLE public.questionnaire_influencers_type_of_contents__type_of_contents_qu OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 25363)
-- Name: questionnaire_influencers_type_of_contents__type_of_cont_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_influencers_type_of_contents__type_of_cont_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_influencers_type_of_contents__type_of_cont_id_seq OWNER TO postgres;

--
-- TOC entry 4286 (class 0 OID 0)
-- Dependencies: 244
-- Name: questionnaire_influencers_type_of_contents__type_of_cont_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_influencers_type_of_contents__type_of_cont_id_seq OWNED BY public.questionnaire_influencers_type_of_contents__type_of_contents_qu.id;


--
-- TOC entry 249 (class 1259 OID 25381)
-- Name: questionnaire_influencers_type_of_non_profit_organisations__typ; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_influencers_type_of_non_profit_organisations__typ (
    id integer NOT NULL,
    "questionnaire-influencer_id" integer,
    "type-of-non-profit-organisation_id" integer
);


ALTER TABLE public.questionnaire_influencers_type_of_non_profit_organisations__typ OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 25379)
-- Name: questionnaire_influencers_type_of_non_profit_organisatio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_influencers_type_of_non_profit_organisatio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_influencers_type_of_non_profit_organisatio_id_seq OWNER TO postgres;

--
-- TOC entry 4287 (class 0 OID 0)
-- Dependencies: 248
-- Name: questionnaire_influencers_type_of_non_profit_organisatio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_influencers_type_of_non_profit_organisatio_id_seq OWNED BY public.questionnaire_influencers_type_of_non_profit_organisations__typ.id;


--
-- TOC entry 223 (class 1259 OID 24744)
-- Name: questionnaire_non_profits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_non_profits (
    id integer NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    "nameOfOrganization" character varying(255),
    "user" integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.questionnaire_non_profits OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 24742)
-- Name: questionnaire_non_profits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_non_profits_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_non_profits_id_seq OWNER TO postgres;

--
-- TOC entry 4288 (class 0 OID 0)
-- Dependencies: 222
-- Name: questionnaire_non_profits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_non_profits_id_seq OWNED BY public.questionnaire_non_profits.id;


--
-- TOC entry 221 (class 1259 OID 24734)
-- Name: range_of_compensations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.range_of_compensations (
    id integer NOT NULL,
    range character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    name character varying(255)
);


ALTER TABLE public.range_of_compensations OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 24732)
-- Name: range_of_compensations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.range_of_compensations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.range_of_compensations_id_seq OWNER TO postgres;

--
-- TOC entry 4289 (class 0 OID 0)
-- Dependencies: 220
-- Name: range_of_compensations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.range_of_compensations_id_seq OWNED BY public.range_of_compensations.id;


--
-- TOC entry 232 (class 1259 OID 24803)
-- Name: social_auth_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.social_auth_tokens (
    id integer NOT NULL,
    google character varying(255),
    amazon character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    questionnaire_influencer integer
);


ALTER TABLE public.social_auth_tokens OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 24801)
-- Name: social_auth_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.social_auth_tokens_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_tokens_id_seq OWNER TO postgres;

--
-- TOC entry 4290 (class 0 OID 0)
-- Dependencies: 231
-- Name: social_auth_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.social_auth_tokens_id_seq OWNED BY public.social_auth_tokens.id;


--
-- TOC entry 238 (class 1259 OID 24836)
-- Name: sponsor_donations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsor_donations (
    id integer NOT NULL,
    questionnaire_corporate integer,
    "promptOnPlatform" boolean,
    "totalAmount" bigint,
    "howShouldItLook" text,
    "callToAction" text,
    "extraToKnow" text,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.sponsor_donations OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 24834)
-- Name: sponsor_donations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sponsor_donations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sponsor_donations_id_seq OWNER TO postgres;

--
-- TOC entry 4291 (class 0 OID 0)
-- Dependencies: 237
-- Name: sponsor_donations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sponsor_donations_id_seq OWNED BY public.sponsor_donations.id;


--
-- TOC entry 255 (class 1259 OID 25412)
-- Name: sponsors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsors (
    id integer NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    organisation character varying(255),
    "user" integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.sponsors OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 25410)
-- Name: sponsors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sponsors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sponsors_id_seq OWNER TO postgres;

--
-- TOC entry 4292 (class 0 OID 0)
-- Dependencies: 254
-- Name: sponsors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sponsors_id_seq OWNED BY public.sponsors.id;


--
-- TOC entry 259 (class 1259 OID 25454)
-- Name: sponsors_type_of_influencers__type_of_influencers_sponsors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsors_type_of_influencers__type_of_influencers_sponsors (
    id integer NOT NULL,
    sponsor_id integer,
    "type-of-influencer_id" integer
);


ALTER TABLE public.sponsors_type_of_influencers__type_of_influencers_sponsors OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 25452)
-- Name: sponsors_type_of_influencers__type_of_influencers_sponso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sponsors_type_of_influencers__type_of_influencers_sponso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sponsors_type_of_influencers__type_of_influencers_sponso_id_seq OWNER TO postgres;

--
-- TOC entry 4293 (class 0 OID 0)
-- Dependencies: 258
-- Name: sponsors_type_of_influencers__type_of_influencers_sponso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sponsors_type_of_influencers__type_of_influencers_sponso_id_seq OWNED BY public.sponsors_type_of_influencers__type_of_influencers_sponsors.id;


--
-- TOC entry 279 (class 1259 OID 26244)
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_profit_o; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o (
    id integer NOT NULL,
    sponsor_id integer,
    "type-of-non-profit-organisation_id" integer
);


ALTER TABLE public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 26242)
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq OWNER TO postgres;

--
-- TOC entry 4294 (class 0 OID 0)
-- Dependencies: 278
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq OWNED BY public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o.id;


--
-- TOC entry 213 (class 1259 OID 24651)
-- Name: strapi_administrator; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.strapi_administrator (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    "resetPasswordToken" character varying(255),
    blocked boolean
);


ALTER TABLE public.strapi_administrator OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24615)
-- Name: strapi_administrator_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.strapi_administrator_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.strapi_administrator_id_seq OWNER TO postgres;

--
-- TOC entry 4295 (class 0 OID 0)
-- Dependencies: 206
-- Name: strapi_administrator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.strapi_administrator_id_seq OWNED BY public.strapi_administrator.id;


--
-- TOC entry 209 (class 1259 OID 24638)
-- Name: strapi_webhooks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.strapi_webhooks (
    id integer NOT NULL,
    name character varying(255),
    url text,
    headers jsonb,
    events jsonb,
    enabled boolean
);


ALTER TABLE public.strapi_webhooks OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24607)
-- Name: strapi_webhooks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.strapi_webhooks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.strapi_webhooks_id_seq OWNER TO postgres;

--
-- TOC entry 4296 (class 0 OID 0)
-- Dependencies: 202
-- Name: strapi_webhooks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.strapi_webhooks_id_seq OWNED BY public.strapi_webhooks.id;


--
-- TOC entry 236 (class 1259 OID 24826)
-- Name: target_campaigns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.target_campaigns (
    id integer NOT NULL,
    target character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    proposal integer
);


ALTER TABLE public.target_campaigns OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 24824)
-- Name: target_campaigns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.target_campaigns_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.target_campaigns_id_seq OWNER TO postgres;

--
-- TOC entry 4297 (class 0 OID 0)
-- Dependencies: 235
-- Name: target_campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.target_campaigns_id_seq OWNED BY public.target_campaigns.id;


--
-- TOC entry 219 (class 1259 OID 24724)
-- Name: type_of_contents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_of_contents (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    questionnaire_non_profit integer,
    questionnaire_influencer integer,
    questionnaire_corporate integer
);


ALTER TABLE public.type_of_contents OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 24722)
-- Name: type_of_contents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_of_contents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_of_contents_id_seq OWNER TO postgres;

--
-- TOC entry 4298 (class 0 OID 0)
-- Dependencies: 218
-- Name: type_of_contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_of_contents_id_seq OWNED BY public.type_of_contents.id;


--
-- TOC entry 257 (class 1259 OID 25427)
-- Name: type_of_influencers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_of_influencers (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.type_of_influencers OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 25425)
-- Name: type_of_influencers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_of_influencers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_of_influencers_id_seq OWNER TO postgres;

--
-- TOC entry 4299 (class 0 OID 0)
-- Dependencies: 256
-- Name: type_of_influencers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_of_influencers_id_seq OWNED BY public.type_of_influencers.id;


--
-- TOC entry 271 (class 1259 OID 26198)
-- Name: type_of_infs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_of_infs (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.type_of_infs OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 26186)
-- Name: type_of_infs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_of_infs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_of_infs_id_seq OWNER TO postgres;

--
-- TOC entry 4300 (class 0 OID 0)
-- Dependencies: 268
-- Name: type_of_infs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_of_infs_id_seq OWNED BY public.type_of_infs.id;


--
-- TOC entry 217 (class 1259 OID 24714)
-- Name: type_of_non_profit_organisations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_of_non_profit_organisations (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    questionnaire_non_profit integer,
    questionnaire_influencer integer,
    questionnaire_corporate integer
);


ALTER TABLE public.type_of_non_profit_organisations OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 24712)
-- Name: type_of_non_profit_organisations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_of_non_profit_organisations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_of_non_profit_organisations_id_seq OWNER TO postgres;

--
-- TOC entry 4301 (class 0 OID 0)
-- Dependencies: 216
-- Name: type_of_non_profit_organisations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_of_non_profit_organisations_id_seq OWNED BY public.type_of_non_profit_organisations.id;


--
-- TOC entry 270 (class 1259 OID 26189)
-- Name: type_of_npos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_of_npos (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.type_of_npos OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 26187)
-- Name: type_of_npos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_of_npos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_of_npos_id_seq OWNER TO postgres;

--
-- TOC entry 4302 (class 0 OID 0)
-- Dependencies: 269
-- Name: type_of_npos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_of_npos_id_seq OWNED BY public.type_of_npos.id;


--
-- TOC entry 208 (class 1259 OID 24630)
-- Name: upload_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.upload_file (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    "alternativeText" character varying(255),
    caption character varying(255),
    width integer,
    height integer,
    formats jsonb,
    hash character varying(255) NOT NULL,
    ext character varying(255),
    mime character varying(255) NOT NULL,
    size numeric(10,2) NOT NULL,
    url character varying(255) NOT NULL,
    "previewUrl" character varying(255),
    provider character varying(255) NOT NULL,
    provider_metadata jsonb,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.upload_file OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24605)
-- Name: upload_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.upload_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.upload_file_id_seq OWNER TO postgres;

--
-- TOC entry 4303 (class 0 OID 0)
-- Dependencies: 201
-- Name: upload_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.upload_file_id_seq OWNED BY public.upload_file.id;


--
-- TOC entry 215 (class 1259 OID 24702)
-- Name: upload_file_morph; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.upload_file_morph (
    id integer NOT NULL,
    upload_file_id integer,
    related_id integer,
    related_type text,
    field text,
    "order" integer
);


ALTER TABLE public.upload_file_morph OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24700)
-- Name: upload_file_morph_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.upload_file_morph_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.upload_file_morph_id_seq OWNER TO postgres;

--
-- TOC entry 4304 (class 0 OID 0)
-- Dependencies: 214
-- Name: upload_file_morph_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.upload_file_morph_id_seq OWNED BY public.upload_file_morph.id;


--
-- TOC entry 229 (class 1259 OID 24783)
-- Name: user_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_types (
    id integer NOT NULL,
    type character varying(255),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    name character varying(255)
);


ALTER TABLE public.user_types OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 24781)
-- Name: user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_types_id_seq OWNER TO postgres;

--
-- TOC entry 4305 (class 0 OID 0)
-- Dependencies: 228
-- Name: user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_types_id_seq OWNED BY public.user_types.id;


--
-- TOC entry 199 (class 1259 OID 24600)
-- Name: users-permissions_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."users-permissions_permission" (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    controller character varying(255) NOT NULL,
    action character varying(255) NOT NULL,
    enabled boolean NOT NULL,
    policy character varying(255),
    role integer
);


ALTER TABLE public."users-permissions_permission" OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24598)
-- Name: users-permissions_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."users-permissions_permission_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."users-permissions_permission_id_seq" OWNER TO postgres;

--
-- TOC entry 4306 (class 0 OID 0)
-- Dependencies: 198
-- Name: users-permissions_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."users-permissions_permission_id_seq" OWNED BY public."users-permissions_permission".id;


--
-- TOC entry 212 (class 1259 OID 24648)
-- Name: users-permissions_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."users-permissions_role" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255)
);


ALTER TABLE public."users-permissions_role" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 24613)
-- Name: users-permissions_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."users-permissions_role_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."users-permissions_role_id_seq" OWNER TO postgres;

--
-- TOC entry 4307 (class 0 OID 0)
-- Dependencies: 205
-- Name: users-permissions_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."users-permissions_role_id_seq" OWNED BY public."users-permissions_role".id;


--
-- TOC entry 197 (class 1259 OID 24589)
-- Name: users-permissions_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."users-permissions_user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    provider character varying(255),
    password character varying(255),
    "resetPasswordToken" character varying(255),
    confirmed boolean,
    blocked boolean,
    role integer,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    "hasSubmitQuestionnaire" boolean,
    user_type integer,
    social_auth_token integer,
    influencer integer,
    non_profit integer,
    sponsor integer
);


ALTER TABLE public."users-permissions_user" OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24587)
-- Name: users-permissions_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."users-permissions_user_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."users-permissions_user_id_seq" OWNER TO postgres;

--
-- TOC entry 4308 (class 0 OID 0)
-- Dependencies: 196
-- Name: users-permissions_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."users-permissions_user_id_seq" OWNED BY public."users-permissions_user".id;


--
-- TOC entry 4018 (class 2604 OID 25506)
-- Name: campaigns id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campaigns ALTER COLUMN id SET DEFAULT nextval('public.campaigns_id_seq'::regclass);


--
-- TOC entry 3960 (class 2604 OID 24677)
-- Name: core_store id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.core_store ALTER COLUMN id SET DEFAULT nextval('public.core_store_id_seq'::regclass);


--
-- TOC entry 3950 (class 2604 OID 24624)
-- Name: influencers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers ALTER COLUMN id SET DEFAULT nextval('public.influencers_id_seq'::regclass);


--
-- TOC entry 4037 (class 2604 OID 26231)
-- Name: influencers_type_of_influencers__type_of_influencers_influencer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers_type_of_influencers__type_of_influencers_influencer ALTER COLUMN id SET DEFAULT nextval('public.influencers_type_of_influencers__type_of_influencers_inf_id_seq'::regclass);


--
-- TOC entry 4040 (class 2604 OID 26255)
-- Name: influencers_type_of_non_profit_organisations__type_of_non_profi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers_type_of_non_profit_organisations__type_of_non_profi ALTER COLUMN id SET DEFAULT nextval('public.influencers_type_of_non_profit_organisations__type_of_no_id_seq'::regclass);


--
-- TOC entry 4005 (class 2604 OID 25394)
-- Name: non_profits id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits ALTER COLUMN id SET DEFAULT nextval('public.non_profits_id_seq'::regclass);


--
-- TOC entry 4038 (class 2604 OID 26239)
-- Name: non_profits_type_of_influencers__type_of_influencers_non_profit id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits_type_of_influencers__type_of_influencers_non_profit ALTER COLUMN id SET DEFAULT nextval('public.non_profits_type_of_influencers__type_of_influencers_non_id_seq'::regclass);


--
-- TOC entry 4041 (class 2604 OID 26263)
-- Name: non_profits_type_of_non_profit_organisations__type_of_non_profi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits_type_of_non_profit_organisations__type_of_non_profi ALTER COLUMN id SET DEFAULT nextval('public.non_profits_type_of_non_profit_organisations__type_of_no_id_seq'::regclass);


--
-- TOC entry 3985 (class 2604 OID 24874)
-- Name: organizationlist id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizationlist ALTER COLUMN id SET DEFAULT nextval('public.organizationlist_id_seq'::regclass);


--
-- TOC entry 3957 (class 2604 OID 24663)
-- Name: organizations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizations ALTER COLUMN id SET DEFAULT nextval('public.organizations_id_seq'::regclass);


--
-- TOC entry 4021 (class 2604 OID 25516)
-- Name: payment_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_statuses ALTER COLUMN id SET DEFAULT nextval('public.payment_statuses_id_seq'::regclass);


--
-- TOC entry 4027 (class 2604 OID 25539)
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- TOC entry 3989 (class 2604 OID 24819)
-- Name: proposal_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_statuses ALTER COLUMN id SET DEFAULT nextval('public.proposal_statuses_id_seq'::regclass);


--
-- TOC entry 4008 (class 2604 OID 25407)
-- Name: proposal_visibilities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_visibilities ALTER COLUMN id SET DEFAULT nextval('public.proposal_visibilities_id_seq'::regclass);


--
-- TOC entry 3998 (class 2604 OID 24852)
-- Name: proposals id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposals ALTER COLUMN id SET DEFAULT nextval('public.proposals_id_seq'::regclass);


--
-- TOC entry 4036 (class 2604 OID 26223)
-- Name: proposals_proposal_visibilities__proposal_visibilities_proposal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposals_proposal_visibilities__proposal_visibilities_proposal ALTER COLUMN id SET DEFAULT nextval('public.proposals_proposal_visibilities__proposal_visibilities_p_id_seq'::regclass);


--
-- TOC entry 4024 (class 2604 OID 25526)
-- Name: public_donations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.public_donations ALTER COLUMN id SET DEFAULT nextval('public.public_donations_id_seq'::regclass);


--
-- TOC entry 3979 (class 2604 OID 24773)
-- Name: questionnaire_corporates id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_corporates_id_seq'::regclass);


--
-- TOC entry 4001 (class 2604 OID 25360)
-- Name: questionnaire_corporates_type_of_contents__type_of_contents_que id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates_type_of_contents__type_of_contents_que ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_corporates_type_of_contents__type_of_conte_id_seq'::regclass);


--
-- TOC entry 4003 (class 2604 OID 25376)
-- Name: questionnaire_corporates_type_of_non_profit_organisations__type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates_type_of_non_profit_organisations__type ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_corporates_type_of_non_profit_organisation_id_seq'::regclass);


--
-- TOC entry 3976 (class 2604 OID 24760)
-- Name: questionnaire_influencers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_influencers_id_seq'::regclass);


--
-- TOC entry 4002 (class 2604 OID 25368)
-- Name: questionnaire_influencers_type_of_contents__type_of_contents_qu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers_type_of_contents__type_of_contents_qu ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_influencers_type_of_contents__type_of_cont_id_seq'::regclass);


--
-- TOC entry 4004 (class 2604 OID 25384)
-- Name: questionnaire_influencers_type_of_non_profit_organisations__typ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers_type_of_non_profit_organisations__typ ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_influencers_type_of_non_profit_organisatio_id_seq'::regclass);


--
-- TOC entry 3973 (class 2604 OID 24747)
-- Name: questionnaire_non_profits id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_non_profits ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_non_profits_id_seq'::regclass);


--
-- TOC entry 3970 (class 2604 OID 24737)
-- Name: range_of_compensations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.range_of_compensations ALTER COLUMN id SET DEFAULT nextval('public.range_of_compensations_id_seq'::regclass);


--
-- TOC entry 3986 (class 2604 OID 24806)
-- Name: social_auth_tokens id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_tokens ALTER COLUMN id SET DEFAULT nextval('public.social_auth_tokens_id_seq'::regclass);


--
-- TOC entry 3995 (class 2604 OID 24839)
-- Name: sponsor_donations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_donations ALTER COLUMN id SET DEFAULT nextval('public.sponsor_donations_id_seq'::regclass);


--
-- TOC entry 4011 (class 2604 OID 25415)
-- Name: sponsors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors ALTER COLUMN id SET DEFAULT nextval('public.sponsors_id_seq'::regclass);


--
-- TOC entry 4017 (class 2604 OID 25457)
-- Name: sponsors_type_of_influencers__type_of_influencers_sponsors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors_type_of_influencers__type_of_influencers_sponsors ALTER COLUMN id SET DEFAULT nextval('public.sponsors_type_of_influencers__type_of_influencers_sponso_id_seq'::regclass);


--
-- TOC entry 4039 (class 2604 OID 26247)
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_profit_o id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o ALTER COLUMN id SET DEFAULT nextval('public.sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq'::regclass);


--
-- TOC entry 3962 (class 2604 OID 24654)
-- Name: strapi_administrator id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strapi_administrator ALTER COLUMN id SET DEFAULT nextval('public.strapi_administrator_id_seq'::regclass);


--
-- TOC entry 3956 (class 2604 OID 24658)
-- Name: strapi_webhooks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strapi_webhooks ALTER COLUMN id SET DEFAULT nextval('public.strapi_webhooks_id_seq'::regclass);


--
-- TOC entry 3992 (class 2604 OID 24829)
-- Name: target_campaigns id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.target_campaigns ALTER COLUMN id SET DEFAULT nextval('public.target_campaigns_id_seq'::regclass);


--
-- TOC entry 3967 (class 2604 OID 24727)
-- Name: type_of_contents id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_contents ALTER COLUMN id SET DEFAULT nextval('public.type_of_contents_id_seq'::regclass);


--
-- TOC entry 4014 (class 2604 OID 25430)
-- Name: type_of_influencers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_influencers ALTER COLUMN id SET DEFAULT nextval('public.type_of_influencers_id_seq'::regclass);


--
-- TOC entry 4033 (class 2604 OID 26201)
-- Name: type_of_infs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_infs ALTER COLUMN id SET DEFAULT nextval('public.type_of_infs_id_seq'::regclass);


--
-- TOC entry 3964 (class 2604 OID 24717)
-- Name: type_of_non_profit_organisations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_non_profit_organisations ALTER COLUMN id SET DEFAULT nextval('public.type_of_non_profit_organisations_id_seq'::regclass);


--
-- TOC entry 4030 (class 2604 OID 26192)
-- Name: type_of_npos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_npos ALTER COLUMN id SET DEFAULT nextval('public.type_of_npos_id_seq'::regclass);


--
-- TOC entry 3953 (class 2604 OID 24633)
-- Name: upload_file id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upload_file ALTER COLUMN id SET DEFAULT nextval('public.upload_file_id_seq'::regclass);


--
-- TOC entry 3963 (class 2604 OID 24705)
-- Name: upload_file_morph id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upload_file_morph ALTER COLUMN id SET DEFAULT nextval('public.upload_file_morph_id_seq'::regclass);


--
-- TOC entry 3982 (class 2604 OID 24786)
-- Name: user_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_types ALTER COLUMN id SET DEFAULT nextval('public.user_types_id_seq'::regclass);


--
-- TOC entry 3949 (class 2604 OID 24617)
-- Name: users-permissions_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_permission" ALTER COLUMN id SET DEFAULT nextval('public."users-permissions_permission_id_seq"'::regclass);


--
-- TOC entry 3961 (class 2604 OID 24659)
-- Name: users-permissions_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_role" ALTER COLUMN id SET DEFAULT nextval('public."users-permissions_role_id_seq"'::regclass);


--
-- TOC entry 3946 (class 2604 OID 24592)
-- Name: users-permissions_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_user" ALTER COLUMN id SET DEFAULT nextval('public."users-permissions_user_id_seq"'::regclass);


--
-- TOC entry 4113 (class 2606 OID 25510)
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- TOC entry 4057 (class 2606 OID 24693)
-- Name: core_store core_store_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.core_store
    ADD CONSTRAINT core_store_pkey PRIMARY KEY (id);


--
-- TOC entry 4049 (class 2606 OID 24683)
-- Name: influencers influencers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers
    ADD CONSTRAINT influencers_pkey PRIMARY KEY (id);


--
-- TOC entry 4127 (class 2606 OID 26233)
-- Name: influencers_type_of_influencers__type_of_influencers_influencer influencers_type_of_influencers__type_of_influencers_influ_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers_type_of_influencers__type_of_influencers_influencer
    ADD CONSTRAINT influencers_type_of_influencers__type_of_influencers_influ_pkey PRIMARY KEY (id);


--
-- TOC entry 4133 (class 2606 OID 26257)
-- Name: influencers_type_of_non_profit_organisations__type_of_non_profi influencers_type_of_non_profit_organisations__type_of_non__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.influencers_type_of_non_profit_organisations__type_of_non_profi
    ADD CONSTRAINT influencers_type_of_non_profit_organisations__type_of_non__pkey PRIMARY KEY (id);


--
-- TOC entry 4103 (class 2606 OID 25401)
-- Name: non_profits non_profits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits
    ADD CONSTRAINT non_profits_pkey PRIMARY KEY (id);


--
-- TOC entry 4129 (class 2606 OID 26241)
-- Name: non_profits_type_of_influencers__type_of_influencers_non_profit non_profits_type_of_influencers__type_of_influencers_non_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits_type_of_influencers__type_of_influencers_non_profit
    ADD CONSTRAINT non_profits_type_of_influencers__type_of_influencers_non_p_pkey PRIMARY KEY (id);


--
-- TOC entry 4135 (class 2606 OID 26265)
-- Name: non_profits_type_of_non_profit_organisations__type_of_non_profi non_profits_type_of_non_profit_organisations__type_of_non__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.non_profits_type_of_non_profit_organisations__type_of_non_profi
    ADD CONSTRAINT non_profits_type_of_non_profit_organisations__type_of_non__pkey PRIMARY KEY (id);


--
-- TOC entry 4083 (class 2606 OID 24876)
-- Name: organizationlist organizationlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizationlist
    ADD CONSTRAINT organizationlist_pkey PRIMARY KEY (id);


--
-- TOC entry 4055 (class 2606 OID 24678)
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);


--
-- TOC entry 4115 (class 2606 OID 25520)
-- Name: payment_statuses payment_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_statuses
    ADD CONSTRAINT payment_statuses_pkey PRIMARY KEY (id);


--
-- TOC entry 4119 (class 2606 OID 25543)
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- TOC entry 4087 (class 2606 OID 24823)
-- Name: proposal_statuses proposal_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_statuses
    ADD CONSTRAINT proposal_statuses_pkey PRIMARY KEY (id);


--
-- TOC entry 4105 (class 2606 OID 25422)
-- Name: proposal_visibilities proposal_visibilities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_visibilities
    ADD CONSTRAINT proposal_visibilities_pkey PRIMARY KEY (id);


--
-- TOC entry 4093 (class 2606 OID 24859)
-- Name: proposals proposals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposals
    ADD CONSTRAINT proposals_pkey PRIMARY KEY (id);


--
-- TOC entry 4125 (class 2606 OID 26225)
-- Name: proposals_proposal_visibilities__proposal_visibilities_proposal proposals_proposal_visibilities__proposal_visibilities_pro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposals_proposal_visibilities__proposal_visibilities_proposal
    ADD CONSTRAINT proposals_proposal_visibilities__proposal_visibilities_pro_pkey PRIMARY KEY (id);


--
-- TOC entry 4117 (class 2606 OID 25533)
-- Name: public_donations public_donations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.public_donations
    ADD CONSTRAINT public_donations_pkey PRIMARY KEY (id);


--
-- TOC entry 4079 (class 2606 OID 24780)
-- Name: questionnaire_corporates questionnaire_corporates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates
    ADD CONSTRAINT questionnaire_corporates_pkey PRIMARY KEY (id);


--
-- TOC entry 4095 (class 2606 OID 25362)
-- Name: questionnaire_corporates_type_of_contents__type_of_contents_que questionnaire_corporates_type_of_contents__type_of_content_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates_type_of_contents__type_of_contents_que
    ADD CONSTRAINT questionnaire_corporates_type_of_contents__type_of_content_pkey PRIMARY KEY (id);


--
-- TOC entry 4099 (class 2606 OID 25378)
-- Name: questionnaire_corporates_type_of_non_profit_organisations__type questionnaire_corporates_type_of_non_profit_organisations__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_corporates_type_of_non_profit_organisations__type
    ADD CONSTRAINT questionnaire_corporates_type_of_non_profit_organisations__pkey PRIMARY KEY (id);


--
-- TOC entry 4077 (class 2606 OID 24767)
-- Name: questionnaire_influencers questionnaire_influencers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers
    ADD CONSTRAINT questionnaire_influencers_pkey PRIMARY KEY (id);


--
-- TOC entry 4097 (class 2606 OID 25370)
-- Name: questionnaire_influencers_type_of_contents__type_of_contents_qu questionnaire_influencers_type_of_contents__type_of_conten_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers_type_of_contents__type_of_contents_qu
    ADD CONSTRAINT questionnaire_influencers_type_of_contents__type_of_conten_pkey PRIMARY KEY (id);


--
-- TOC entry 4101 (class 2606 OID 25386)
-- Name: questionnaire_influencers_type_of_non_profit_organisations__typ questionnaire_influencers_type_of_non_profit_organisations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_influencers_type_of_non_profit_organisations__typ
    ADD CONSTRAINT questionnaire_influencers_type_of_non_profit_organisations_pkey PRIMARY KEY (id);


--
-- TOC entry 4075 (class 2606 OID 24754)
-- Name: questionnaire_non_profits questionnaire_non_profits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_non_profits
    ADD CONSTRAINT questionnaire_non_profits_pkey PRIMARY KEY (id);


--
-- TOC entry 4073 (class 2606 OID 24741)
-- Name: range_of_compensations range_of_compensations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.range_of_compensations
    ADD CONSTRAINT range_of_compensations_pkey PRIMARY KEY (id);


--
-- TOC entry 4085 (class 2606 OID 24813)
-- Name: social_auth_tokens social_auth_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_tokens
    ADD CONSTRAINT social_auth_tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 4091 (class 2606 OID 24846)
-- Name: sponsor_donations sponsor_donations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_donations
    ADD CONSTRAINT sponsor_donations_pkey PRIMARY KEY (id);


--
-- TOC entry 4107 (class 2606 OID 25424)
-- Name: sponsors sponsors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors
    ADD CONSTRAINT sponsors_pkey PRIMARY KEY (id);


--
-- TOC entry 4111 (class 2606 OID 25459)
-- Name: sponsors_type_of_influencers__type_of_influencers_sponsors sponsors_type_of_influencers__type_of_influencers_sponsors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors_type_of_influencers__type_of_influencers_sponsors
    ADD CONSTRAINT sponsors_type_of_influencers__type_of_influencers_sponsors_pkey PRIMARY KEY (id);


--
-- TOC entry 4131 (class 2606 OID 26249)
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_profit_o sponsors_type_of_non_profit_organisations__type_of_non_pro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o
    ADD CONSTRAINT sponsors_type_of_non_profit_organisations__type_of_non_pro_pkey PRIMARY KEY (id);


--
-- TOC entry 4063 (class 2606 OID 24691)
-- Name: strapi_administrator strapi_administrator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strapi_administrator
    ADD CONSTRAINT strapi_administrator_pkey PRIMARY KEY (id);


--
-- TOC entry 4065 (class 2606 OID 24697)
-- Name: strapi_administrator strapi_administrator_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strapi_administrator
    ADD CONSTRAINT strapi_administrator_username_unique UNIQUE (username);


--
-- TOC entry 4053 (class 2606 OID 24687)
-- Name: strapi_webhooks strapi_webhooks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strapi_webhooks
    ADD CONSTRAINT strapi_webhooks_pkey PRIMARY KEY (id);


--
-- TOC entry 4089 (class 2606 OID 24833)
-- Name: target_campaigns target_campaigns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.target_campaigns
    ADD CONSTRAINT target_campaigns_pkey PRIMARY KEY (id);


--
-- TOC entry 4071 (class 2606 OID 24731)
-- Name: type_of_contents type_of_contents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_contents
    ADD CONSTRAINT type_of_contents_pkey PRIMARY KEY (id);


--
-- TOC entry 4109 (class 2606 OID 25434)
-- Name: type_of_influencers type_of_influencers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_influencers
    ADD CONSTRAINT type_of_influencers_pkey PRIMARY KEY (id);


--
-- TOC entry 4123 (class 2606 OID 26205)
-- Name: type_of_infs type_of_infs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_infs
    ADD CONSTRAINT type_of_infs_pkey PRIMARY KEY (id);


--
-- TOC entry 4069 (class 2606 OID 24721)
-- Name: type_of_non_profit_organisations type_of_non_profit_organisations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_non_profit_organisations
    ADD CONSTRAINT type_of_non_profit_organisations_pkey PRIMARY KEY (id);


--
-- TOC entry 4121 (class 2606 OID 26196)
-- Name: type_of_npos type_of_npos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_of_npos
    ADD CONSTRAINT type_of_npos_pkey PRIMARY KEY (id);


--
-- TOC entry 4067 (class 2606 OID 24710)
-- Name: upload_file_morph upload_file_morph_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upload_file_morph
    ADD CONSTRAINT upload_file_morph_pkey PRIMARY KEY (id);


--
-- TOC entry 4051 (class 2606 OID 24681)
-- Name: upload_file upload_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upload_file
    ADD CONSTRAINT upload_file_pkey PRIMARY KEY (id);


--
-- TOC entry 4081 (class 2606 OID 24790)
-- Name: user_types user_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_types
    ADD CONSTRAINT user_types_pkey PRIMARY KEY (id);


--
-- TOC entry 4047 (class 2606 OID 24680)
-- Name: users-permissions_permission users-permissions_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_permission"
    ADD CONSTRAINT "users-permissions_permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 4059 (class 2606 OID 24685)
-- Name: users-permissions_role users-permissions_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_role"
    ADD CONSTRAINT "users-permissions_role_pkey" PRIMARY KEY (id);


--
-- TOC entry 4061 (class 2606 OID 24695)
-- Name: users-permissions_role users-permissions_role_type_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_role"
    ADD CONSTRAINT "users-permissions_role_type_unique" UNIQUE (type);


--
-- TOC entry 4043 (class 2606 OID 24682)
-- Name: users-permissions_user users-permissions_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_user"
    ADD CONSTRAINT "users-permissions_user_pkey" PRIMARY KEY (id);


--
-- TOC entry 4045 (class 2606 OID 24699)
-- Name: users-permissions_user users-permissions_user_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."users-permissions_user"
    ADD CONSTRAINT "users-permissions_user_username_unique" UNIQUE (username);


-- Completed on 2020-07-22 20:05:41 UTC

--
-- PostgreSQL database dump complete
--

