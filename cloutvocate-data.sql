--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

-- Started on 2020-07-22 20:06:01 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4322 (class 0 OID 25503)
-- Dependencies: 261
-- Data for Name: campaigns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.campaigns (id, "youtubeVideoLink", proposal, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4272 (class 0 OID 24644)
-- Dependencies: 211
-- Data for Name: core_store; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.core_store (id, key, value, type, environment, tag) FROM stdin;
1	db_model_strapi_webhooks	{"name":{"type":"string"},"url":{"type":"text"},"headers":{"type":"json"},"events":{"type":"json"},"enabled":{"type":"boolean"}}	object	\N	\N
2	db_model_organizations	{"name":{"type":"string"},"email":{"type":"string"},"websiteUrl":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
76	db_model_type_of_npos	{"name":{"type":"string"},"non_profits":{"via":"type_of_non_profit_organisations","collection":"non-profit","attribute":"non-profit","column":"id","isVirtual":true},"influencers":{"via":"type_of_non_profit_organisations","collection":"influencer","attribute":"influencer","column":"id","isVirtual":true},"sponsors":{"via":"type_of_non_profit_organisations","collection":"sponsor","attribute":"sponsor","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
4	db_model_upload_file	{"name":{"type":"string","configurable":false,"required":true},"alternativeText":{"type":"string","configurable":false},"caption":{"type":"string","configurable":false},"width":{"type":"integer","configurable":false},"height":{"type":"integer","configurable":false},"formats":{"type":"json","configurable":false},"hash":{"type":"string","configurable":false,"required":true},"ext":{"type":"string","configurable":false},"mime":{"type":"string","configurable":false,"required":true},"size":{"type":"decimal","configurable":false,"required":true},"url":{"type":"string","configurable":false,"required":true},"previewUrl":{"type":"string","configurable":false},"provider":{"type":"string","configurable":false,"required":true},"provider_metadata":{"type":"json","configurable":false},"related":{"collection":"*","filter":"field","configurable":false},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
5	db_model_users-permissions_permission	{"type":{"type":"string","required":true,"configurable":false},"controller":{"type":"string","required":true,"configurable":false},"action":{"type":"string","required":true,"configurable":false},"enabled":{"type":"boolean","required":true,"configurable":false},"policy":{"type":"string","configurable":false},"role":{"model":"role","via":"permissions","plugin":"users-permissions","configurable":false}}	object	\N	\N
6	db_model_core_store	{"key":{"type":"string"},"value":{"type":"text"},"type":{"type":"string"},"environment":{"type":"string"},"tag":{"type":"string"}}	object	\N	\N
7	db_model_users-permissions_role	{"name":{"type":"string","minLength":3,"required":true,"configurable":false},"description":{"type":"string","configurable":false},"type":{"type":"string","unique":true,"configurable":false},"permissions":{"collection":"permission","via":"role","plugin":"users-permissions","configurable":false,"isVirtual":true},"users":{"collection":"user","via":"role","configurable":false,"plugin":"users-permissions","isVirtual":true}}	object	\N	\N
8	db_model_strapi_administrator	{"username":{"type":"string","minLength":3,"unique":true,"configurable":false,"required":true},"email":{"type":"email","minLength":6,"configurable":false,"required":true},"password":{"type":"password","minLength":6,"configurable":false,"private":true,"required":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"blocked":{"type":"boolean","default":false,"configurable":false}}	object	\N	\N
10	db_model_upload_file_morph	{"upload_file_id":{"type":"integer"},"related_id":{"type":"integer"},"related_type":{"type":"text"},"field":{"type":"text"},"order":{"type":"integer"}}	object	\N	\N
11	plugin_upload_settings	{"sizeOptimization":true,"responsiveDimensions":true}	object	development	
12	plugin_users-permissions_grant	{"email":{"enabled":true,"icon":"envelope"},"discord":{"enabled":false,"icon":"discord","key":"","secret":"","callback":"/auth/discord/callback","scope":["identify","email"]},"facebook":{"enabled":false,"icon":"facebook-square","key":"","secret":"","callback":"/auth/facebook/callback","scope":["email"]},"google":{"enabled":false,"icon":"google","key":"","secret":"","callback":"/auth/google/callback","scope":["email"]},"github":{"enabled":false,"icon":"github","key":"","secret":"","callback":"/auth/github/callback","scope":["user","user:email"]},"microsoft":{"enabled":false,"icon":"windows","key":"","secret":"","callback":"/auth/microsoft/callback","scope":["user.read"]},"twitter":{"enabled":false,"icon":"twitter","key":"","secret":"","callback":"/auth/twitter/callback"},"instagram":{"enabled":false,"icon":"instagram","key":"","secret":"","callback":"/auth/instagram/callback"},"vk":{"enabled":false,"icon":"vk","key":"","secret":"","callback":"/auth/vk/callback","scope":["email"]},"twitch":{"enabled":false,"icon":"twitch","key":"","secret":"","callback":"/auth/twitch/callback","scope":["user:read:email"]}}	object		
15	plugin_content_manager_configuration_content_types::plugins::upload.file	{"uid":"plugins::upload.file","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"alternativeText":{"edit":{"label":"AlternativeText","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AlternativeText","searchable":true,"sortable":true}},"caption":{"edit":{"label":"Caption","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Caption","searchable":true,"sortable":true}},"width":{"edit":{"label":"Width","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Width","searchable":true,"sortable":true}},"height":{"edit":{"label":"Height","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Height","searchable":true,"sortable":true}},"formats":{"edit":{"label":"Formats","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Formats","searchable":false,"sortable":false}},"hash":{"edit":{"label":"Hash","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Hash","searchable":true,"sortable":true}},"ext":{"edit":{"label":"Ext","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Ext","searchable":true,"sortable":true}},"mime":{"edit":{"label":"Mime","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Mime","searchable":true,"sortable":true}},"size":{"edit":{"label":"Size","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Size","searchable":true,"sortable":true}},"url":{"edit":{"label":"Url","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Url","searchable":true,"sortable":true}},"previewUrl":{"edit":{"label":"PreviewUrl","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PreviewUrl","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"provider_metadata":{"edit":{"label":"Provider_metadata","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider_metadata","searchable":false,"sortable":false}},"related":{"edit":{"label":"Related","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"Related","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","alternativeText","caption"],"editRelations":["related"],"edit":[[{"name":"name","size":6},{"name":"alternativeText","size":6}],[{"name":"caption","size":6},{"name":"width","size":4}],[{"name":"height","size":4}],[{"name":"formats","size":12}],[{"name":"hash","size":6},{"name":"ext","size":6}],[{"name":"mime","size":6},{"name":"size","size":4}],[{"name":"url","size":6},{"name":"previewUrl","size":6}],[{"name":"provider","size":6}],[{"name":"provider_metadata","size":12}]]}}	object		
77	db_model_type_of_infs	{"name":{"type":"string"},"influencers":{"via":"type_of_influencers","collection":"influencer","attribute":"influencer","column":"id","isVirtual":true},"non_profits":{"via":"type_of_influencers","collection":"non-profit","attribute":"non-profit","column":"id","isVirtual":true},"sponsors":{"via":"type_of_influencers","collection":"sponsor","attribute":"sponsor","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
16	plugin_content_manager_configuration_content_types::plugins::users-permissions.permission	{"uid":"plugins::users-permissions.permission","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"type","defaultSortBy":"type","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"controller":{"edit":{"label":"Controller","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Controller","searchable":true,"sortable":true}},"action":{"edit":{"label":"Action","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Action","searchable":true,"sortable":true}},"enabled":{"edit":{"label":"Enabled","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Enabled","searchable":true,"sortable":true}},"policy":{"edit":{"label":"Policy","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Policy","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":false,"sortable":false}}},"layouts":{"list":["id","type","controller","action"],"editRelations":["role"],"edit":[[{"name":"type","size":6},{"name":"controller","size":6}],[{"name":"action","size":6},{"name":"enabled","size":4}],[{"name":"policy","size":6}]]}}	object		
17	plugin_content_manager_configuration_content_types::plugins::users-permissions.role	{"uid":"plugins::users-permissions.role","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"permissions":{"edit":{"label":"Permissions","description":"","placeholder":"","visible":true,"editable":true,"mainField":"type"},"list":{"label":"Permissions","searchable":false,"sortable":false}},"users":{"edit":{"label":"Users","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Users","searchable":false,"sortable":false}}},"layouts":{"list":["id","name","description","type"],"editRelations":["permissions","users"],"edit":[[{"name":"name","size":6},{"name":"description","size":6}],[{"name":"type","size":6}]]}}	object		
18	plugin_content_manager_configuration_content_types::strapi::administrator	{"uid":"strapi::administrator","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"username","defaultSortBy":"username","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}}},"layouts":{"list":["id","username","email","blocked"],"editRelations":[],"edit":[[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"blocked","size":4}]]}}	object		
20	plugin_users-permissions_email	{"reset_password":{"display":"Email.template.reset_password","icon":"sync","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Reset password","message":"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>"}},"email_confirmation":{"display":"Email.template.email_confirmation","icon":"check-square","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Account confirmation","message":"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>"}}}	object		
21	plugin_users-permissions_advanced	{"unique_email":true,"allow_register":true,"email_confirmation":false,"email_confirmation_redirection":"/admin/admin","email_reset_password":"/admin/admin","default_role":"authenticated"}	object		
22	db_model_type_of_non_profit_organisations	{"name":{"type":"string"},"non_profits":{"via":"type_of_non_profit_organisations","collection":"non-profit","attribute":"non-profit","column":"id","isVirtual":true},"influencers":{"via":"type_of_non_profit_organisations","collection":"influencer","attribute":"influencer","column":"id","isVirtual":true},"sponsors":{"via":"type_of_non_profit_organisations","collection":"sponsor","attribute":"sponsor","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
27	plugin_content_manager_configuration_content_types::application::range-of-compensation.range-of-compensation	{"uid":"application::range-of-compensation.range-of-compensation","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at","name"],"edit":[[{"name":"name","size":6}]],"editRelations":[]}}	object		
28	db_model_questionnaire_non_profits	{"firstName":{"type":"string"},"lastName":{"type":"string"},"type_of_contents":{"collection":"type-of-content","via":"questionnaire_non_profit","isVirtual":true},"nameOfOrganization":{"type":"string"},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"questionnaire_non_profit","isVirtual":true},"user":{"plugin":"users-permissions","model":"user"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
19	plugin_content_manager_configuration_content_types::plugins::users-permissions.user	{"uid":"plugins::users-permissions.user","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"username","defaultSortBy":"username","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"confirmed":{"edit":{"label":"Confirmed","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Confirmed","searchable":true,"sortable":true}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":false,"sortable":false}},"influencer":{"edit":{"label":"Influencer","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Influencer","searchable":false,"sortable":false}},"non_profit":{"edit":{"label":"Non_profit","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Non_profit","searchable":false,"sortable":false}},"sponsor":{"edit":{"label":"Sponsor","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Sponsor","searchable":false,"sortable":false}},"user_type":{"edit":{"label":"User_type","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"User_type","searchable":false,"sortable":false}},"hasSubmitQuestionnaire":{"edit":{"label":"HasSubmitQuestionnaire","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"HasSubmitQuestionnaire","searchable":true,"sortable":true}},"proposals":{"edit":{"label":"Proposals","description":"","placeholder":"","visible":true,"editable":true,"mainField":"campaignName"},"list":{"label":"Proposals","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","username","email","confirmed"],"edit":[[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"confirmed","size":4}],[{"name":"blocked","size":4},{"name":"hasSubmitQuestionnaire","size":4}]],"editRelations":["role","user_type","influencer","non_profit","sponsor","proposals"]}}	object		
26	db_model_range_of_compensations	{"name":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
24	db_model_type_of_contents	{"name":{"type":"string"},"questionnaire_non_profit":{"via":"type_of_contents","model":"questionnaire-non-profit"},"questionnaire_influencers":{"via":"type_of_contents","collection":"questionnaire-influencer","attribute":"questionnaire-influencer","column":"id","isVirtual":true},"questionnaire_corporates":{"via":"type_of_contents","collection":"questionnaire-corporate","attribute":"questionnaire-corporate","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
58	db_model_sponsors_type_of_non_profit_organisations__type_of_non_profit_organisations_sponsors	{"sponsor_id":{"type":"integer"},"type-of-non-profit-organisation_id":{"type":"integer"}}	object	\N	\N
62	plugin_content_manager_configuration_content_types::application::influencer.influencer	{"uid":"application::influencer.influencer","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"firstName","defaultSortBy":"firstName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"firstName":{"edit":{"label":"FirstName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"FirstName","searchable":true,"sortable":true}},"lastName":{"edit":{"label":"LastName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"LastName","searchable":true,"sortable":true}},"interestedInDonating":{"edit":{"label":"InterestedInDonating","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"InterestedInDonating","searchable":true,"sortable":true}},"type_of_influencers":{"edit":{"label":"Type_of_influencers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_influencers","searchable":false,"sortable":false}},"type_of_non_profit_organisations":{"edit":{"label":"Type_of_non_profit_organisations","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_non_profit_organisations","searchable":false,"sortable":false}},"range_of_compensation":{"edit":{"label":"Range_of_compensation","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Range_of_compensation","searchable":false,"sortable":false}},"user":{"edit":{"label":"User","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"User","searchable":false,"sortable":false}},"googleAuthToken":{"edit":{"label":"GoogleAuthToken","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"GoogleAuthToken","searchable":true,"sortable":true}},"amazonAuthToken":{"edit":{"label":"AmazonAuthToken","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AmazonAuthToken","searchable":true,"sortable":true}},"googlePhotoUrl":{"edit":{"label":"GooglePhotoUrl","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"GooglePhotoUrl","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","firstName","lastName","interestedInDonating"],"editRelations":["type_of_influencers","type_of_non_profit_organisations","range_of_compensation","user"],"edit":[[{"name":"firstName","size":6},{"name":"lastName","size":6}],[{"name":"interestedInDonating","size":4},{"name":"googleAuthToken","size":6}],[{"name":"amazonAuthToken","size":6},{"name":"googlePhotoUrl","size":6}]]}}	object		
32	db_model_questionnaire_corporates	{"firstName":{"type":"string"},"lastName":{"type":"string"},"type_of_contents":{"collection":"type-of-content","via":"questionnaire_corporates","dominant":true,"attribute":"type-of-content","column":"id","isVirtual":true},"name":{"type":"string"},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"questionnaire_corporates","dominant":true,"attribute":"type-of-non-profit-organisation","column":"id","isVirtual":true},"user":{"plugin":"users-permissions","model":"user"},"sponsor_donation":{"model":"sponsor-donation","via":"questionnaire_corporate"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
36	db_model_social_auth_tokens	{"google":{"type":"string"},"amazon":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
40	db_model_target_campaigns	{"target":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
42	db_model_sponsor_donations	{"promptOnPlatform":{"type":"boolean"},"totalAmount":{"type":"biginteger"},"howShouldItLook":{"type":"text"},"callToAction":{"type":"text"},"extraToKnow":{"type":"text"},"questionnaire_corporate":{"via":"sponsor_donation","model":"questionnaire-corporate"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
34	db_model_user_types	{"name":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
38	db_model_proposal_statuses	{"name":{"type":"string"},"color":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
35	plugin_content_manager_configuration_content_types::application::user-type.user-type	{"uid":"application::user-type.user-type","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at","name"],"edit":[[{"name":"name","size":6}]],"editRelations":[]}}	object		
48	db_model_questionnaire_corporates_type_of_contents__type_of_contents_questionnaire_corporates	{"questionnaire-corporate_id":{"type":"integer"},"type-of-content_id":{"type":"integer"}}	object	\N	\N
46	db_model_questionnaire_influencers_type_of_contents__type_of_contents_questionnaire_influencers	{"questionnaire-influencer_id":{"type":"integer"},"type-of-content_id":{"type":"integer"}}	object	\N	\N
49	db_model_questionnaire_corporates_type_of_non_profit_organisations__type_of_non_profit_organisations_questionnaire_corporates	{"questionnaire-corporate_id":{"type":"integer"},"type-of-non-profit-organisation_id":{"type":"integer"}}	object	\N	\N
47	db_model_questionnaire_influencers_type_of_non_profit_organisations__type_of_non_profit_organisations_questionnaire_influencers	{"questionnaire-influencer_id":{"type":"integer"},"type-of-non-profit-organisation_id":{"type":"integer"}}	object	\N	\N
39	plugin_content_manager_configuration_content_types::application::proposal-status.proposal-status	{"uid":"application::proposal-status.proposal-status","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"color":{"edit":{"label":"Color","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Color","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at","name"],"edit":[[{"name":"name","size":6},{"name":"color","size":6}]],"editRelations":[]}}	object		
50	db_model_proposals_questionnaire_influencers__questionnaire_influencers_proposals	{"questionnaire-influencer_id":{"type":"integer"},"proposal_id":{"type":"integer"}}	object	\N	\N
30	db_model_questionnaire_influencers	{"firstName":{"type":"string"},"lastName":{"type":"string"},"type_of_contents":{"collection":"type-of-content","via":"questionnaire_influencers","dominant":true,"attribute":"type-of-content","column":"id","isVirtual":true},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"questionnaire_influencers","dominant":true,"attribute":"type-of-non-profit-organisation","column":"id","isVirtual":true},"interestedInDonating":{"type":"boolean"},"range_of_compensation":{"model":"range-of-compensation"},"user":{"plugin":"users-permissions","model":"user"},"social_auth_token":{"model":"social-auth-token"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
51	db_model_proposal_visibilities	{"name":{"type":"string"},"proposals":{"via":"proposal_visibilities","collection":"proposal","attribute":"proposal","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
54	db_model_type_of_influencers	{"name":{"type":"string"},"influencers":{"via":"type_of_influencers","collection":"influencer","attribute":"influencer","column":"id","isVirtual":true},"non_profits":{"via":"type_of_influencers","collection":"non-profit","attribute":"non-profit","column":"id","isVirtual":true},"sponsors":{"via":"type_of_influencers","collection":"sponsor","attribute":"sponsor","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
56	db_model_sponsors_type_of_influencers__type_of_influencers_sponsors	{"sponsor_id":{"type":"integer"},"type-of-influencer_id":{"type":"integer"}}	object	\N	\N
66	plugin_content_manager_configuration_content_types::application::sponsor.sponsor	{"uid":"application::sponsor.sponsor","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"firstName","defaultSortBy":"firstName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"firstName":{"edit":{"label":"FirstName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"FirstName","searchable":true,"sortable":true}},"lastName":{"edit":{"label":"LastName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"LastName","searchable":true,"sortable":true}},"type_of_influencers":{"edit":{"label":"Type_of_influencers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_influencers","searchable":false,"sortable":false}},"type_of_non_profit_organisations":{"edit":{"label":"Type_of_non_profit_organisations","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_non_profit_organisations","searchable":false,"sortable":false}},"organisation":{"edit":{"label":"Organisation","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Organisation","searchable":true,"sortable":true}},"user":{"edit":{"label":"User","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"User","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","firstName","lastName","organisation"],"editRelations":["type_of_influencers","type_of_non_profit_organisations","user"],"edit":[[{"name":"firstName","size":6},{"name":"lastName","size":6}],[{"name":"organisation","size":6}]]}}	object		
53	db_model_sponsors	{"firstName":{"type":"string"},"lastName":{"type":"string"},"type_of_influencers":{"collection":"type-of-influencer","via":"sponsors","dominant":true,"attribute":"type-of-influencer","column":"id","isVirtual":true},"organisation":{"type":"string"},"user":{"plugin":"users-permissions","model":"user","via":"sponsor"},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"sponsors","dominant":true,"attribute":"type-of-non-profit-organisation","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
52	db_model_non_profits	{"firstName":{"type":"string"},"lastName":{"type":"string"},"organisation":{"type":"string"},"type_of_influencers":{"collection":"type-of-influencer","via":"non_profits","dominant":true,"attribute":"type-of-influencer","column":"id","isVirtual":true},"user":{"plugin":"users-permissions","model":"user","via":"non_profit"},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"non_profits","dominant":true,"attribute":"type-of-non-profit-organisation","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
57	db_model_non_profits_type_of_influencers__type_of_influencers_non_profits	{"non-profit_id":{"type":"integer"},"type-of-influencer_id":{"type":"integer"}}	object	\N	\N
55	db_model_non_profits_type_of_non_profit_organisations__type_of_non_profit_organisations_non_profits	{"non-profit_id":{"type":"integer"},"type-of-non-profit-organisation_id":{"type":"integer"}}	object	\N	\N
67	plugin_content_manager_configuration_content_types::application::type-of-influencer.type-of-influencer	{"uid":"application::type-of-influencer.type-of-influencer","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"influencers":{"edit":{"label":"Influencers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Influencers","searchable":false,"sortable":false}},"non_profits":{"edit":{"label":"Non_profits","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Non_profits","searchable":false,"sortable":false}},"sponsors":{"edit":{"label":"Sponsors","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Sponsors","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","created_at","updated_at"],"editRelations":["influencers","non_profits","sponsors"],"edit":[[{"name":"name","size":6}]]}}	object		
23	plugin_content_manager_configuration_content_types::application::type-of-non-profit-organisation.type-of-non-profit-organisation	{"uid":"application::type-of-non-profit-organisation.type-of-non-profit-organisation","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"influencers":{"edit":{"label":"Influencers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Influencers","searchable":false,"sortable":false}},"non_profits":{"edit":{"label":"Non_profits","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Non_profits","searchable":false,"sortable":false}},"sponsors":{"edit":{"label":"Sponsors","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstName"},"list":{"label":"Sponsors","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","created_at","updated_at"],"edit":[[{"name":"name","size":6}]],"editRelations":["influencers","non_profits","sponsors"]}}	object		
3	db_model_influencers	{"firstName":{"type":"string"},"lastName":{"type":"string"},"interestedInDonating":{"type":"boolean"},"type_of_influencers":{"collection":"type-of-influencer","via":"influencers","dominant":true,"attribute":"type-of-influencer","column":"id","isVirtual":true},"range_of_compensation":{"model":"range-of-compensation"},"user":{"plugin":"users-permissions","model":"user","via":"influencer"},"googleAuthToken":{"type":"text"},"amazonAuthToken":{"type":"text"},"googlePhotoUrl":{"type":"string"},"type_of_non_profit_organisations":{"collection":"type-of-non-profit-organisation","via":"influencers","dominant":true,"attribute":"type-of-non-profit-organisation","column":"id","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
63	plugin_content_manager_configuration_content_types::application::non-profit.non-profit	{"uid":"application::non-profit.non-profit","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"firstName","defaultSortBy":"firstName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"firstName":{"edit":{"label":"FirstName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"FirstName","searchable":true,"sortable":true}},"lastName":{"edit":{"label":"LastName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"LastName","searchable":true,"sortable":true}},"organisation":{"edit":{"label":"Organisation","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Organisation","searchable":true,"sortable":true}},"type_of_influencers":{"edit":{"label":"Type_of_influencers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_influencers","searchable":false,"sortable":false}},"user":{"edit":{"label":"User","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"User","searchable":false,"sortable":false}},"type_of_non_profit_organisations":{"edit":{"label":"Type_of_non_profit_organisations","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_non_profit_organisations","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","firstName","lastName","organisation"],"edit":[[{"name":"firstName","size":6},{"name":"lastName","size":6}],[{"name":"organisation","size":6}]],"editRelations":["type_of_non_profit_organisations","type_of_influencers","user"]}}	object		
59	db_model_influencers_type_of_influencers__type_of_influencers_influencers	{"influencer_id":{"type":"integer"},"type-of-influencer_id":{"type":"integer"}}	object	\N	\N
61	db_model_proposals_proposal_visibilities__proposal_visibilities_proposals	{"proposal_id":{"type":"integer"},"proposal-visibility_id":{"type":"integer"}}	object	\N	\N
60	db_model_influencers_type_of_non_profit_organisations__type_of_non_profit_organisations_influencers	{"influencer_id":{"type":"integer"},"type-of-non-profit-organisation_id":{"type":"integer"}}	object	\N	\N
64	plugin_content_manager_configuration_content_types::application::proposal-visibility.proposal-visibility	{"uid":"application::proposal-visibility.proposal-visibility","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"proposals":{"edit":{"label":"Proposals","description":"","placeholder":"","visible":true,"editable":true,"mainField":"campaignName"},"list":{"label":"Proposals","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","created_at","updated_at"],"editRelations":["proposals"],"edit":[[{"name":"name","size":6}]]}}	object		
68	db_model_campaigns	{"youtubeVideoLink":{"type":"string"},"proposal":{"via":"campaign","model":"proposal"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
69	db_model_payment_statuses	{"name":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
70	db_model_public_donations	{"payment":{"model":"payment"},"firstName":{"type":"string"},"lastName":{"type":"string"},"emailAddress":{"type":"string"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
71	db_model_payments	{"from":{"plugin":"users-permissions","model":"user"},"to":{"plugin":"users-permissions","model":"user"},"amount":{"type":"float"},"payment_status":{"model":"payment-status"},"proposal":{"model":"proposal"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
9	db_model_users-permissions_user	{"username":{"type":"string","minLength":3,"unique":true,"configurable":false,"required":true},"email":{"type":"email","minLength":6,"configurable":false,"required":true},"provider":{"type":"string","configurable":false},"password":{"type":"password","minLength":6,"configurable":false,"private":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"confirmed":{"type":"boolean","default":false,"configurable":false},"blocked":{"type":"boolean","default":false,"configurable":false},"role":{"model":"role","via":"users","plugin":"users-permissions","configurable":false},"influencer":{"via":"user","model":"influencer"},"non_profit":{"via":"user","model":"non-profit"},"sponsor":{"via":"user","model":"sponsor"},"user_type":{"model":"user-type"},"hasSubmitQuestionnaire":{"type":"boolean"},"proposals":{"via":"non_profit","collection":"proposal","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
44	db_model_proposals	{"non_profit":{"plugin":"users-permissions","model":"user"},"campaignName":{"type":"string"},"campaignDescription":{"type":"text"},"campaignStartDate":{"type":"date"},"campaignEndDate":{"type":"date"},"proposalApprovalDate":{"type":"date"},"proposal_visibilities":{"collection":"proposal-visibility","via":"proposals","dominant":true,"attribute":"proposal-visibility","column":"id","isVirtual":true},"howShouldItLookNonProfit":{"type":"text"},"callToActionNonProfit":{"type":"text"},"sponsorshipIntegration":{"type":"boolean"},"nonprofitOrganizationPromotion":{"type":"boolean"},"sendOnlyToInfluencers":{"type":"boolean"},"taxReceipt":{"type":"boolean"},"anyThingElseNonProfit":{"type":"text"},"statusInfluencer":{"model":"proposal-status"},"percentRevenueInfluencer":{"type":"integer"},"anythingElseInfluencer":{"type":"text"},"influencer":{"plugin":"users-permissions","model":"user"},"statusSponsor":{"model":"proposal-status"},"budgetSponsor":{"type":"integer"},"howShouldItLookSponsor":{"type":"text"},"callToActionSponsor":{"type":"text"},"anyThingElseSponsor":{"type":"text"},"statusInfluencerWithSponsor":{"model":"proposal-status"},"statusNonProfitWithSponsor":{"model":"proposal-status"},"isACampaign":{"type":"boolean"},"promotingCampaignSponsor":{"type":"boolean"},"sponsor":{"plugin":"users-permissions","model":"user"},"campaign":{"model":"campaign","via":"proposal"},"paymentInfluencer":{"model":"payment"},"paymentStoI":{"model":"payment"},"paymentStoNP":{"model":"payment"},"hasCampaignStarted":{"type":"boolean"},"hasCampaignEnded":{"type":"boolean"},"type_of_non_profit_organisation":{"model":"type-of-non-profit-organisation"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}	object	\N	\N
72	plugin_content_manager_configuration_content_types::application::campaign.campaign	{"uid":"application::campaign.campaign","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"youtubeVideoLink","defaultSortBy":"youtubeVideoLink","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"youtubeVideoLink":{"edit":{"label":"YoutubeVideoLink","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"YoutubeVideoLink","searchable":true,"sortable":true}},"proposal":{"edit":{"label":"Proposal","description":"","placeholder":"","visible":true,"editable":true,"mainField":"campaignName"},"list":{"label":"Proposal","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","youtubeVideoLink","created_at","updated_at"],"editRelations":["proposal"],"edit":[[{"name":"youtubeVideoLink","size":6}]]}}	object		
73	plugin_content_manager_configuration_content_types::application::payment-status.payment-status	{"uid":"application::payment-status.payment-status","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","created_at","updated_at"],"editRelations":[],"edit":[[{"name":"name","size":6}]]}}	object		
74	plugin_content_manager_configuration_content_types::application::payment.payment	{"uid":"application::payment.payment","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"from":{"edit":{"label":"From","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"From","searchable":false,"sortable":false}},"to":{"edit":{"label":"To","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"To","searchable":false,"sortable":false}},"amount":{"edit":{"label":"Amount","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Amount","searchable":true,"sortable":true}},"payment_status":{"edit":{"label":"Payment_status","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Payment_status","searchable":false,"sortable":false}},"proposal":{"edit":{"label":"Proposal","description":"","placeholder":"","visible":true,"editable":true,"mainField":"campaignName"},"list":{"label":"Proposal","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","amount","created_at","updated_at"],"editRelations":["from","to","payment_status","proposal"],"edit":[[{"name":"amount","size":4}]]}}	object		
75	plugin_content_manager_configuration_content_types::application::public-donation.public-donation	{"uid":"application::public-donation.public-donation","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"firstName","defaultSortBy":"firstName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"payment":{"edit":{"label":"Payment","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"Payment","searchable":false,"sortable":false}},"firstName":{"edit":{"label":"FirstName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"FirstName","searchable":true,"sortable":true}},"lastName":{"edit":{"label":"LastName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"LastName","searchable":true,"sortable":true}},"emailAddress":{"edit":{"label":"EmailAddress","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"EmailAddress","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","firstName","lastName","emailAddress"],"editRelations":["payment"],"edit":[[{"name":"firstName","size":6},{"name":"lastName","size":6}],[{"name":"emailAddress","size":6}]]}}	object		
65	plugin_content_manager_configuration_content_types::application::proposal.proposal	{"uid":"application::proposal.proposal","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"campaignName","defaultSortBy":"campaignName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"non_profit":{"edit":{"label":"Non_profit","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Non_profit","searchable":false,"sortable":false}},"campaignName":{"edit":{"label":"CampaignName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CampaignName","searchable":true,"sortable":true}},"campaignDescription":{"edit":{"label":"CampaignDescription","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CampaignDescription","searchable":true,"sortable":true}},"campaignStartDate":{"edit":{"label":"CampaignStartDate","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CampaignStartDate","searchable":true,"sortable":true}},"campaignEndDate":{"edit":{"label":"CampaignEndDate","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CampaignEndDate","searchable":true,"sortable":true}},"proposalApprovalDate":{"edit":{"label":"ProposalApprovalDate","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ProposalApprovalDate","searchable":true,"sortable":true}},"proposal_visibilities":{"edit":{"label":"Proposal_visibilities","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Proposal_visibilities","searchable":false,"sortable":false}},"howShouldItLookNonProfit":{"edit":{"label":"HowShouldItLookNonProfit","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"HowShouldItLookNonProfit","searchable":true,"sortable":true}},"callToActionNonProfit":{"edit":{"label":"CallToActionNonProfit","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CallToActionNonProfit","searchable":true,"sortable":true}},"sponsorshipIntegration":{"edit":{"label":"SponsorshipIntegration","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"SponsorshipIntegration","searchable":true,"sortable":true}},"nonprofitOrganizationPromotion":{"edit":{"label":"NonprofitOrganizationPromotion","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"NonprofitOrganizationPromotion","searchable":true,"sortable":true}},"sendOnlyToInfluencers":{"edit":{"label":"SendOnlyToInfluencers","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"SendOnlyToInfluencers","searchable":true,"sortable":true}},"taxReceipt":{"edit":{"label":"TaxReceipt","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"TaxReceipt","searchable":true,"sortable":true}},"anyThingElseNonProfit":{"edit":{"label":"AnyThingElseNonProfit","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AnyThingElseNonProfit","searchable":true,"sortable":true}},"statusInfluencer":{"edit":{"label":"StatusInfluencer","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"StatusInfluencer","searchable":false,"sortable":false}},"percentRevenueInfluencer":{"edit":{"label":"PercentRevenueInfluencer","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PercentRevenueInfluencer","searchable":true,"sortable":true}},"anythingElseInfluencer":{"edit":{"label":"AnythingElseInfluencer","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AnythingElseInfluencer","searchable":true,"sortable":true}},"influencer":{"edit":{"label":"Influencer","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Influencer","searchable":false,"sortable":false}},"statusSponsor":{"edit":{"label":"StatusSponsor","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"StatusSponsor","searchable":false,"sortable":false}},"budgetSponsor":{"edit":{"label":"BudgetSponsor","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"BudgetSponsor","searchable":true,"sortable":true}},"howShouldItLookSponsor":{"edit":{"label":"HowShouldItLookSponsor","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"HowShouldItLookSponsor","searchable":true,"sortable":true}},"callToActionSponsor":{"edit":{"label":"CallToActionSponsor","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"CallToActionSponsor","searchable":true,"sortable":true}},"anyThingElseSponsor":{"edit":{"label":"AnyThingElseSponsor","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AnyThingElseSponsor","searchable":true,"sortable":true}},"statusInfluencerWithSponsor":{"edit":{"label":"StatusInfluencerWithSponsor","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"StatusInfluencerWithSponsor","searchable":false,"sortable":false}},"statusNonProfitWithSponsor":{"edit":{"label":"StatusNonProfitWithSponsor","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"StatusNonProfitWithSponsor","searchable":false,"sortable":false}},"isACampaign":{"edit":{"label":"IsACampaign","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"IsACampaign","searchable":true,"sortable":true}},"promotingCampaignSponsor":{"edit":{"label":"PromotingCampaignSponsor","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PromotingCampaignSponsor","searchable":true,"sortable":true}},"sponsor":{"edit":{"label":"Sponsor","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Sponsor","searchable":false,"sortable":false}},"campaign":{"edit":{"label":"Campaign","description":"","placeholder":"","visible":true,"editable":true,"mainField":"youtubeVideoLink"},"list":{"label":"Campaign","searchable":false,"sortable":false}},"paymentInfluencer":{"edit":{"label":"PaymentInfluencer","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"PaymentInfluencer","searchable":false,"sortable":false}},"paymentStoI":{"edit":{"label":"PaymentStoI","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"PaymentStoI","searchable":false,"sortable":false}},"paymentStoNP":{"edit":{"label":"PaymentStoNP","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"PaymentStoNP","searchable":false,"sortable":false}},"hasCampaignStarted":{"edit":{"label":"HasCampaignStarted","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"HasCampaignStarted","searchable":true,"sortable":true}},"hasCampaignEnded":{"edit":{"label":"HasCampaignEnded","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"HasCampaignEnded","searchable":true,"sortable":true}},"type_of_non_profit_organisation":{"edit":{"label":"Type_of_non_profit_organisation","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Type_of_non_profit_organisation","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","campaignName","campaignDescription","campaignStartDate"],"edit":[[{"name":"campaignName","size":6},{"name":"campaignDescription","size":6}],[{"name":"campaignStartDate","size":4},{"name":"campaignEndDate","size":4},{"name":"proposalApprovalDate","size":4}],[{"name":"howShouldItLookNonProfit","size":6},{"name":"callToActionNonProfit","size":6}],[{"name":"sponsorshipIntegration","size":4},{"name":"nonprofitOrganizationPromotion","size":4},{"name":"sendOnlyToInfluencers","size":4}],[{"name":"taxReceipt","size":4},{"name":"anyThingElseNonProfit","size":6}],[{"name":"percentRevenueInfluencer","size":4},{"name":"anythingElseInfluencer","size":6}],[{"name":"budgetSponsor","size":4},{"name":"howShouldItLookSponsor","size":6}],[{"name":"callToActionSponsor","size":6},{"name":"anyThingElseSponsor","size":6}],[{"name":"isACampaign","size":4},{"name":"promotingCampaignSponsor","size":4},{"name":"hasCampaignStarted","size":4}],[{"name":"hasCampaignEnded","size":4}]],"editRelations":["non_profit","type_of_non_profit_organisation","proposal_visibilities","statusInfluencer","influencer","statusSponsor","statusInfluencerWithSponsor","statusNonProfitWithSponsor","sponsor","campaign","paymentInfluencer","paymentStoI","paymentStoNP"]}}	object		
\.


--
-- TOC entry 4268 (class 0 OID 24621)
-- Dependencies: 207
-- Data for Name: influencers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.influencers (id, name, "instagramUsername", "youtubeChannelUrl", created_at, updated_at, "firstName", "lastName", "interestedInDonating", range_of_compensation, "user", "googleAuthToken", "amazonAuthToken", "googlePhotoUrl") FROM stdin;
9	\N	\N	\N	2020-07-22 19:48:27.841+00	2020-07-22 19:48:27.892+00	test	influencer	t	4	23	ya29.a0AfH6SMDI1H8LKIoEJRzpj1y9S4EhbKyCjWrQw1wM9-h6j01X7MH6_u2icZ705BchvUSyHHR4lvREBnC8C8LU1JB14MgFx0zqjDEWJBTRTGURsdN0_6n0wwoj3HfyaFzJOS4bh0HvJ2N06kKk36acGlrKu6oT9zP_6JHN	\N	https://lh4.googleusercontent.com/-B-Yv35YFjm0/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclBVtfPM3C_Ad8DfrH2obvrR9pJWw/s96-c/photo.jpg
\.


--
-- TOC entry 4336 (class 0 OID 26228)
-- Dependencies: 275
-- Data for Name: influencers_type_of_influencers__type_of_influencers_influencer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.influencers_type_of_influencers__type_of_influencers_influencer (id, influencer_id, "type-of-influencer_id") FROM stdin;
26	9	1
27	9	2
28	9	3
29	9	20
30	9	19
31	9	17
\.


--
-- TOC entry 4342 (class 0 OID 26252)
-- Dependencies: 281
-- Data for Name: influencers_type_of_non_profit_organisations__type_of_non_profi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.influencers_type_of_non_profit_organisations__type_of_non_profi (id, influencer_id, "type-of-non-profit-organisation_id") FROM stdin;
10	6	17
11	6	19
12	6	20
16	7	20
17	7	19
18	7	17
25	9	1
26	9	2
27	9	3
28	9	11
29	9	10
30	9	9
\.


--
-- TOC entry 4312 (class 0 OID 25391)
-- Dependencies: 251
-- Data for Name: non_profits; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.non_profits (id, "firstName", "lastName", organisation, "user", created_at, updated_at) FROM stdin;
3	test	non profit	test non profit org.	24	2020-07-22 19:50:20.892+00	2020-07-22 19:50:20.939+00
\.


--
-- TOC entry 4338 (class 0 OID 26236)
-- Dependencies: 277
-- Data for Name: non_profits_type_of_influencers__type_of_influencers_non_profit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.non_profits_type_of_influencers__type_of_influencers_non_profit (id, "non-profit_id", "type-of-influencer_id") FROM stdin;
13	3	1
14	3	2
15	3	3
16	3	20
17	3	19
18	3	17
\.


--
-- TOC entry 4344 (class 0 OID 26260)
-- Dependencies: 283
-- Data for Name: non_profits_type_of_non_profit_organisations__type_of_non_profi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.non_profits_type_of_non_profit_organisations__type_of_non_profi (id, "non-profit_id", "type-of-non-profit-organisation_id") FROM stdin;
13	3	1
14	3	2
15	3	3
16	3	11
17	3	10
18	3	9
\.


--
-- TOC entry 4291 (class 0 OID 24793)
-- Dependencies: 230
-- Data for Name: organizationlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organizationlist (charitynavigatorurl, charityname, ein, irsclassification, usstate, id) FROM stdin;
https://www.charitynavigator.org/?bay=search.profile&ein=000000003&utm_source=DataAPI&utm_content=ba5f21bf	Boobies Rock!	3	Health 	CO	97
https://www.charitynavigator.org/?bay=search.profile&ein=000000030&utm_source=DataAPI&utm_content=ba5f21bf	Voice of a Child U.S.	30	Education	NY	98
https://www.charitynavigator.org/?bay=search.profile&ein=000000065&utm_source=DataAPI&utm_content=ba5f21bf	Community College Consortium on Autism and Intellectual Disabilities	65	Education	VA	99
https://www.charitynavigator.org/?bay=search.profile&ein=000000080&utm_source=DataAPI&utm_content=ba5f21bf	Juniata Community Mental Health Clinic	80	Human Services 	PA	100
https://www.charitynavigator.org/?bay=search.profile&ein=000000085&utm_source=DataAPI&utm_content=ba5f21bf	One Door for Education, Inc.	85	Education	FL	101
https://www.charitynavigator.org/?bay=search.profile&ein=000000089&utm_source=DataAPI&utm_content=ba5f21bf	SWAG Nation	89	Human Services 	LA	102
https://www.charitynavigator.org/?bay=search.profile&ein=000000099&utm_source=DataAPI&utm_content=ba5f21bf	Good Samaritans of America	99	Human Services 	NJ	103
https://www.charitynavigator.org/?bay=search.profile&ein=000000108&utm_source=DataAPI&utm_content=ba5f21bf	North American Butterfly Association, Manasota Chapter	108	Animals	FL	104
https://www.charitynavigator.org/?bay=search.profile&ein=000000110&utm_source=DataAPI&utm_content=ba5f21bf	Coalition for Veterans of America	110	Human Services 	KY	105
https://www.charitynavigator.org/?bay=search.profile&ein=000000111&utm_source=DataAPI&utm_content=ba5f21bf	We Can-CerVive	111	Human Services 	MD	106
https://www.charitynavigator.org/?bay=search.profile&ein=000000120&utm_source=DataAPI&utm_content=ba5f21bf	Wheels from Valor	120	Human Services 	NC	107
https://www.charitynavigator.org/?bay=search.profile&ein=000000122&utm_source=DataAPI&utm_content=ba5f21bf	H.O.P.E. for Single Parents and Families	122	Human Services 	PA	108
https://www.charitynavigator.org/?bay=search.profile&ein=000000123&utm_source=DataAPI&utm_content=ba5f21bf	Nationwide Soldier Support	123	Human Services 	WA	109
https://www.charitynavigator.org/?bay=search.profile&ein=000000124&utm_source=DataAPI&utm_content=ba5f21bf	Top Dog Rescue, Boarding, and Training	124	Animals	FL	110
https://www.charitynavigator.org/?bay=search.profile&ein=000000125&utm_source=DataAPI&utm_content=ba5f21bf	Cancer Exam Network	125	Human Services 	WA	111
https://www.charitynavigator.org/?bay=search.profile&ein=000000126&utm_source=DataAPI&utm_content=ba5f21bf	Back to School Helping Hands	126	Education	WA	112
https://www.charitynavigator.org/?bay=search.profile&ein=000000127&utm_source=DataAPI&utm_content=ba5f21bf	Holiday Relief Fund	127	Human Services 	WA	113
https://www.charitynavigator.org/?bay=search.profile&ein=000000128&utm_source=DataAPI&utm_content=ba5f21bf	Needy Children's Shopping Spree	128	Human Services 	WA	114
https://www.charitynavigator.org/?bay=search.profile&ein=000000129&utm_source=DataAPI&utm_content=ba5f21bf	Power to Our Vets	129	Human Services 	FL	115
https://www.charitynavigator.org/?bay=search.profile&ein=000000131&utm_source=DataAPI&utm_content=ba5f21bf	Policeman's Fund	131	Human Services 	ME	116
https://www.charitynavigator.org/?bay=search.profile&ein=000000132&utm_source=DataAPI&utm_content=ba5f21bf	Maine Police Alliance	132	Human Services 	ME	117
https://www.charitynavigator.org/?bay=search.profile&ein=000000133&utm_source=DataAPI&utm_content=ba5f21bf	Woodfin Community Fund	133	Community Development	AL	118
https://www.charitynavigator.org/?bay=search.profile&ein=000000134&utm_source=DataAPI&utm_content=ba5f21bf	Wounded Warrior Fund	134	Human Services 	IN	119
https://www.charitynavigator.org/?bay=search.profile&ein=000000135&utm_source=DataAPI&utm_content=ba5f21bf	Wounded Warrior Foundation	135	Human Services 	IN	120
https://www.charitynavigator.org/?bay=search.profile&ein=000000136&utm_source=DataAPI&utm_content=ba5f21bf	Red Cross of Americas, Inc	136	Human Services 	ND	121
https://www.charitynavigator.org/?bay=search.profile&ein=000000137&utm_source=DataAPI&utm_content=ba5f21bf	Officers Down 5k	137	Human Services 	ND	122
https://www.charitynavigator.org/?bay=search.profile&ein=000000138&utm_source=DataAPI&utm_content=ba5f21bf	A Magic City Christmas	138	Human Services 	ND	123
https://www.charitynavigator.org/?bay=search.profile&ein=000000139&utm_source=DataAPI&utm_content=ba5f21bf	National Endowment Association, Inc	139	Community Development	IN	124
https://www.charitynavigator.org/?bay=search.profile&ein=000000140&utm_source=DataAPI&utm_content=ba5f21bf	Indiana Endowment Fund	140	Community Development	IN	125
https://www.charitynavigator.org/?bay=search.profile&ein=000000141&utm_source=DataAPI&utm_content=ba5f21bf	St. Mary's Prayer Center Ministry	141	Religion 	WV	126
https://www.charitynavigator.org/?bay=search.profile&ein=000000143&utm_source=DataAPI&utm_content=ba5f21bf	Wounded Warrior Foundation of Orange County	143	Human Services 	NY	127
https://www.charitynavigator.org/?bay=search.profile&ein=000000144&utm_source=DataAPI&utm_content=ba5f21bf	Veterans of America	144	Human and Civil Rights	UT	128
https://www.charitynavigator.org/?bay=search.profile&ein=000000145&utm_source=DataAPI&utm_content=ba5f21bf	Vehicles for Veterans, LLC.	145	Human and Civil Rights	UT	129
https://www.charitynavigator.org/?bay=search.profile&ein=000000146&utm_source=DataAPI&utm_content=ba5f21bf	Saving Our Soldiers	146	Human and Civil Rights	UT	130
https://www.charitynavigator.org/?bay=search.profile&ein=000000147&utm_source=DataAPI&utm_content=ba5f21bf	Donate Your Car	147	Human Services 	UT	131
https://www.charitynavigator.org/?bay=search.profile&ein=000000148&utm_source=DataAPI&utm_content=ba5f21bf	Donate That Car LLC	148	Human Services 	UT	132
https://www.charitynavigator.org/?bay=search.profile&ein=000000149&utm_source=DataAPI&utm_content=ba5f21bf	Act of Valor	149	Human Services 	UT	133
https://www.charitynavigator.org/?bay=search.profile&ein=000000150&utm_source=DataAPI&utm_content=ba5f21bf	Medal of Honor	150	Human Services 	UT	134
https://www.charitynavigator.org/?bay=search.profile&ein=000000151&utm_source=DataAPI&utm_content=ba5f21bf	Veterans Children Foundation	151	Human and Civil Rights	NJ	135
https://www.charitynavigator.org/?bay=search.profile&ein=000000152&utm_source=DataAPI&utm_content=ba5f21bf	Veterans Relief Association	152	Human Services 	ID	136
https://www.charitynavigator.org/?bay=search.profile&ein=000000153&utm_source=DataAPI&utm_content=ba5f21bf	Focus on Veterans	153	Human Services 	CT	137
https://www.charitynavigator.org/?bay=search.profile&ein=000000154&utm_source=DataAPI&utm_content=ba5f21bf	Hearts2Heroes	154	Human Services 	WV	138
https://www.charitynavigator.org/?bay=search.profile&ein=000000172&utm_source=DataAPI&utm_content=ba5f21bf	Riding for Boobs	172	Health	TX	139
https://www.charitynavigator.org/?bay=search.profile&ein=000000200&utm_source=DataAPI&utm_content=ba5f21bf	Atlanta Human Rights Foundation	200	Human Services 	GA	140
https://www.charitynavigator.org/?bay=search.profile&ein=000000201&utm_source=DataAPI&utm_content=ba5f21bf	American Cancer Society of Washington	201	Health	WA	141
https://www.charitynavigator.org/?bay=search.profile&ein=000000202&utm_source=DataAPI&utm_content=ba5f21bf	American Cancer Society of Seattle	202	Health	WA	142
https://www.charitynavigator.org/?bay=search.profile&ein=000000203&utm_source=DataAPI&utm_content=ba5f21bf	American Red Cross of Seattle	203	Human Services 	WA	143
https://www.charitynavigator.org/?bay=search.profile&ein=000000204&utm_source=DataAPI&utm_content=ba5f21bf	American Red Cross of Washington	204	Human Services 	WA	144
https://www.charitynavigator.org/?bay=search.profile&ein=000000205&utm_source=DataAPI&utm_content=ba5f21bf	United Way of Seattle	205	Community Development	WA	145
https://www.charitynavigator.org/?bay=search.profile&ein=000000206&utm_source=DataAPI&utm_content=ba5f21bf	United Way of Washington	206	Community Development	WA	146
https://www.charitynavigator.org/?bay=search.profile&ein=000000207&utm_source=DataAPI&utm_content=ba5f21bf	Oliver's Rescue Mission	207	Animals	CA	147
https://www.charitynavigator.org/?bay=search.profile&ein=000000210&utm_source=DataAPI&utm_content=ba5f21bf	All Accounted For	210	Animals	MO	148
https://www.charitynavigator.org/?bay=search.profile&ein=000000222&utm_source=DataAPI&utm_content=ba5f21bf	Kids on Safe Streets	222	Human Services 	KS	149
https://www.charitynavigator.org/?bay=search.profile&ein=000000600&utm_source=DataAPI&utm_content=ba5f21bf	Rescue Pets Iowa Corp	600	Animals	IA	150
https://www.charitynavigator.org/?bay=search.profile&ein=000019818&utm_source=DataAPI&utm_content=ba5f21bf	PALMER SECOND BAPTIST CHURCH	19818	Religion 	MA	151
https://www.charitynavigator.org/?bay=search.profile&ein=000029215&utm_source=DataAPI&utm_content=ba5f21bf	ST GEORGE CATHEDRAL	29215	Religion 	MA	152
https://www.charitynavigator.org/?bay=search.profile&ein=000260049&utm_source=DataAPI&utm_content=ba5f21bf	CORINTH BAPTIST CHURCH	260049	Religion 	FL	153
https://www.charitynavigator.org/?bay=search.profile&ein=000360268&utm_source=DataAPI&utm_content=ba5f21bf	IGLESIA VICTORIA	360268	Religion 	MO	154
https://www.charitynavigator.org/?bay=search.profile&ein=000490336&utm_source=DataAPI&utm_content=ba5f21bf	EASTSIDE BAPTIST CHURCH	490336	Religion 	FL	155
https://www.charitynavigator.org/?bay=search.profile&ein=000587764&utm_source=DataAPI&utm_content=ba5f21bf	Iglesia Bethesda Inc.	587764	Religion 	MA	156
https://www.charitynavigator.org/?bay=search.profile&ein=000635913&utm_source=DataAPI&utm_content=ba5f21bf	Ministerio Apostolico Jesucristo Es El Senor Inc.	635913	Religion 	MA	157
https://www.charitynavigator.org/?bay=search.profile&ein=000765634&utm_source=DataAPI&utm_content=ba5f21bf	Mercy Chapel International	765634	Religion 	MA	158
https://www.charitynavigator.org/?bay=search.profile&ein=000841363&utm_source=DataAPI&utm_content=ba5f21bf	Agape House of Prayer	841363	Religion / Community Development	MA	159
https://www.charitynavigator.org/?bay=search.profile&ein=000852649&utm_source=DataAPI&utm_content=ba5f21bf	Bethany Presbyterian Church	852649	Religion 	MA	160
https://www.charitynavigator.org/?bay=search.profile&ein=001028397&utm_source=DataAPI&utm_content=ba5f21bf	UNITY IN THE CITY	1028397	Religion / Education 	MA	161
https://www.charitynavigator.org/?bay=search.profile&ein=002022084&utm_source=DataAPI&utm_content=ba5f21bf	ST ROSALIAS ROMAN CATHOLIC CHURCH	2022084	Religion  / Community Development	NY	162
https://www.charitynavigator.org/?bay=search.profile&ein=002030711&utm_source=DataAPI&utm_content=ba5f21bf	Church of Our Lord Jesus Christ of Macedonia	2030711	Religion 	NJ	163
https://www.charitynavigator.org/?bay=search.profile&ein=002045409&utm_source=DataAPI&utm_content=ba5f21bf	GENERAL COUNCIL OF ASSEMBLIES OF GOD FULL GOSPEL TABERNACLE	2045409	Religion 	NY	164
https://www.charitynavigator.org/?bay=search.profile&ein=002120849&utm_source=DataAPI&utm_content=ba5f21bf	ANCILLA DOMINI SISTERS INC	2120849	Community Development	IN	165
https://www.charitynavigator.org/?bay=search.profile&ein=002296179&utm_source=DataAPI&utm_content=ba5f21bf	Religious Science Church Center of San Diego	2296179	Religion 	CA	166
https://www.charitynavigator.org/?bay=search.profile&ein=003140260&utm_source=DataAPI&utm_content=ba5f21bf	PILGRIM BAPTIST CHURCH	3140260	Religion 	CA	167
https://www.charitynavigator.org/?bay=search.profile&ein=003390819&utm_source=DataAPI&utm_content=ba5f21bf	Rep Resources Empowering Parents	3390819	 Community Development / Human Services	IL	168
https://www.charitynavigator.org/?bay=search.profile&ein=007764840&utm_source=DataAPI&utm_content=ba5f21bf	Tri-County Lighthouse Baptist Church & Ministries Inc.	7764840	Religion 	NJ	169
https://www.charitynavigator.org/?bay=search.profile&ein=010002847&utm_source=DataAPI&utm_content=ba5f21bf	Hulls Cove Neighborhood Association	10002847	Arts, Culture and Humanities	ME	170
https://www.charitynavigator.org/?bay=search.profile&ein=010015091&utm_source=DataAPI&utm_content=ba5f21bf	Hanover Soccer Club Inc.	10015091	 Community Development	NJ	171
https://www.charitynavigator.org/?bay=search.profile&ein=010017496&utm_source=DataAPI&utm_content=ba5f21bf	Agamenticus Yacht Club of York	10017496	Education 	ME	172
https://www.charitynavigator.org/?bay=search.profile&ein=010018555&utm_source=DataAPI&utm_content=ba5f21bf	ALPHA TAU OMEGA FRATERNITY	10018555	Community Development	ME	173
https://www.charitynavigator.org/?bay=search.profile&ein=010018605&utm_source=DataAPI&utm_content=ba5f21bf	AMALGAMATED TRANSIT UNION	10018605	Human Services 	ME	174
https://www.charitynavigator.org/?bay=search.profile&ein=010018922&utm_source=DataAPI&utm_content=ba5f21bf	AMERICAN LEGION	10018922	Human and Civil Rights	ME	175
https://www.charitynavigator.org/?bay=search.profile&ein=010018923&utm_source=DataAPI&utm_content=ba5f21bf	AMERICAN LEGION POST 0019 THOMAS W COLE POST	10018923	Human and Civil Rights	ME	176
https://www.charitynavigator.org/?bay=search.profile&ein=010018927&utm_source=DataAPI&utm_content=ba5f21bf	AMERICAN LEGION POST 0005 BOURQUE LANIGAN	10018927	Human and Civil Rights	ME	177
https://www.charitynavigator.org/?bay=search.profile&ein=010018930&utm_source=DataAPI&utm_content=ba5f21bf	AMERICAN LEGION	10018930	Human and Civil Rights	ME	178
https://www.charitynavigator.org/?bay=search.profile&ein=010019705&utm_source=DataAPI&utm_content=ba5f21bf	Ancient Free & Accepted Masons of Maine Grand Lodge	10019705	Community Development	ME	179
https://www.charitynavigator.org/?bay=search.profile&ein=010019708&utm_source=DataAPI&utm_content=ba5f21bf	ANCIENT FREE & ACCEPTED MASONS OF MAINE	10019708	Community Development	ME	180
https://www.charitynavigator.org/?bay=search.profile&ein=010021545&utm_source=DataAPI&utm_content=ba5f21bf	MAINE STATE CHAMBER OF COMMERCE	10021545	Research and Public Policy	ME	181
https://www.charitynavigator.org/?bay=search.profile&ein=010022320&utm_source=DataAPI&utm_content=ba5f21bf	AUGUSTA COUNTRY CLUB	10022320	Arts, Culture and Humanities	ME	182
https://www.charitynavigator.org/?bay=search.profile&ein=010022415&utm_source=DataAPI&utm_content=ba5f21bf	KENNEBEC VALLEY CHAMBER OF COMMERCE	10022415	Community Development	ME	183
https://www.charitynavigator.org/?bay=search.profile&ein=010024155&utm_source=DataAPI&utm_content=ba5f21bf	Bangor Band	10024155	Community Development	ME	184
https://www.charitynavigator.org/?bay=search.profile&ein=010024245&utm_source=DataAPI&utm_content=ba5f21bf	BANGOR REGION CHAMBER OF COMMERCE	10024245	Research and Public Policy	ME	185
https://www.charitynavigator.org/?bay=search.profile&ein=010024455&utm_source=DataAPI&utm_content=ba5f21bf	BENEVOLENT & PROTECTIVE ORDER OF ELKS OF THE USA	10024455	Community Development	ME	186
https://www.charitynavigator.org/?bay=search.profile&ein=010024645&utm_source=DataAPI&utm_content=ba5f21bf	Bangor Symphony Orchestra	10024645	Arts, Culture and Humanities	ME	187
https://www.charitynavigator.org/?bay=search.profile&ein=010024907&utm_source=DataAPI&utm_content=ba5f21bf	Bar Harbor Village Improvement Association	10024907	Community Development	ME	188
https://www.charitynavigator.org/?bay=search.profile&ein=010027415&utm_source=DataAPI&utm_content=ba5f21bf	ANCIENT FREE & ACCEPTED MASONS OF MAINE	10027415	Community Development	ME	189
https://www.charitynavigator.org/?bay=search.profile&ein=010027741&utm_source=DataAPI&utm_content=ba5f21bf	BENEVOLENT & PROTECTIVE ORDER OF ELKS OF THE USA	10027741	Community Development	ME	190
https://www.charitynavigator.org/?bay=search.profile&ein=010028850&utm_source=DataAPI&utm_content=ba5f21bf	Berwick Cemetery Association Inc.	10028850	Human services	ME	191
https://www.charitynavigator.org/?bay=search.profile&ein=010028980&utm_source=DataAPI&utm_content=ba5f21bf	BETA THETA PI FRATERNITY	10028980	Community Development	OH	192
\.


--
-- TOC entry 4271 (class 0 OID 24641)
-- Dependencies: 210
-- Data for Name: organizations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organizations (id, name, email, "websiteUrl", created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4324 (class 0 OID 25513)
-- Dependencies: 263
-- Data for Name: payment_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_statuses (id, name, created_at, updated_at) FROM stdin;
1	completed	2020-07-22 13:34:01.642+00	2020-07-22 13:34:01.642+00
2	due	2020-07-22 13:34:07.888+00	2020-07-22 13:34:07.888+00
3	public	2020-07-22 13:34:17.439+00	2020-07-22 13:34:17.439+00
\.


--
-- TOC entry 4328 (class 0 OID 25536)
-- Dependencies: 267
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payments (id, "from", "to", amount, payment_status, proposal, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4295 (class 0 OID 24816)
-- Dependencies: 234
-- Data for Name: proposal_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proposal_statuses (id, status, created_at, updated_at, proposal, name, color) FROM stdin;
1	\N	2020-07-22 13:34:37.061+00	2020-07-22 13:34:37.061+00	\N	Accepted	#58c5af
2	\N	2020-07-22 13:35:00.05+00	2020-07-22 13:35:00.05+00	\N	read	#0099d9
3	\N	2020-07-22 13:36:43.584+00	2020-07-22 13:36:43.584+00	\N	Awaiting	#0099d9
4	\N	2020-07-22 13:37:01.186+00	2020-07-22 13:37:01.186+00	\N	declined	#3a3a3a
5	\N	2020-07-22 13:37:22.804+00	2020-07-22 13:37:22.804+00	\N	Unread	#ff9900
6	\N	2020-07-22 13:37:35.137+00	2020-07-22 13:37:35.137+00	\N	closed	#3a3a3a
7	\N	2020-07-22 13:37:46.773+00	2020-07-22 13:37:46.773+00	\N	open	#3a3a3a
\.


--
-- TOC entry 4314 (class 0 OID 25404)
-- Dependencies: 253
-- Data for Name: proposal_visibilities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proposal_visibilities (id, name, created_at, updated_at) FROM stdin;
1	facebook	2020-07-22 13:38:15.531+00	2020-07-22 13:38:15.531+00
2	instagram	2020-07-22 13:38:23.232+00	2020-07-22 13:38:23.232+00
3	tiktok	2020-07-22 13:38:32.173+00	2020-07-22 13:38:32.173+00
4	snapchat	2020-07-22 13:38:42.403+00	2020-07-22 13:38:42.403+00
5	youtube	2020-07-22 13:38:49.183+00	2020-07-22 13:38:49.183+00
\.


--
-- TOC entry 4301 (class 0 OID 24849)
-- Dependencies: 240
-- Data for Name: proposals; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proposals (id, questionnaire_non_profit, questionnaire_influencer, proposal_status, sponsor_donation, "campaignName", type_of_non_profit_organisation, "campaignDescription", "campaignStartDate", "CampaignEndDate", "ProposalApprovalDate", target_campaign, "howShouldItLook", calltoaction, "allowSponsor", "promptingCampaignOnPlatform", "sendOnlyToGivingBack", "offerTaxReceipt", "extraInformation", created_at, updated_at, non_profit, "campaignEndDate", "proposalApprovalDate", "howShouldItLookNonProfit", "callToActionNonProfit", "sponsorshipIntegration", "nonprofitOrganizationPromotion", "sendOnlyToInfluencers", "taxReceipt", "anyThingElseNonProfit", "statusInfluencer", "percentRevenueInfluencer", "anythingElseInfluencer", influencer, "statusSponsor", "budgetSponsor", "howShouldItLookSponsor", "callToActionSponsor", "anyThingElseSponsor", "statusInfluencerWithSponsor", "statusNonProfitWithSponsor", "isACampaign", "promotingCampaignSponsor", sponsor, campaign, "paymentInfluencer", "paymentStoI", "paymentStoNP", "hasCampaignStarted", "hasCampaignEnded") FROM stdin;
\.


--
-- TOC entry 4334 (class 0 OID 26220)
-- Dependencies: 273
-- Data for Name: proposals_proposal_visibilities__proposal_visibilities_proposal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proposals_proposal_visibilities__proposal_visibilities_proposal (id, proposal_id, "proposal-visibility_id") FROM stdin;
\.


--
-- TOC entry 4326 (class 0 OID 25523)
-- Dependencies: 265
-- Data for Name: public_donations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.public_donations (id, payment, "firstName", "lastName", "emailAddress", created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4288 (class 0 OID 24770)
-- Dependencies: 227
-- Data for Name: questionnaire_corporates; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_corporates (id, "firstName", "lastName", name, "user", created_at, updated_at, sponsor_donation) FROM stdin;
\.


--
-- TOC entry 4304 (class 0 OID 25357)
-- Dependencies: 243
-- Data for Name: questionnaire_corporates_type_of_contents__type_of_contents_que; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_corporates_type_of_contents__type_of_contents_que (id, "questionnaire-corporate_id", "type-of-content_id") FROM stdin;
\.


--
-- TOC entry 4308 (class 0 OID 25373)
-- Dependencies: 247
-- Data for Name: questionnaire_corporates_type_of_non_profit_organisations__type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_corporates_type_of_non_profit_organisations__type (id, "questionnaire-corporate_id", "type-of-non-profit-organisation_id") FROM stdin;
\.


--
-- TOC entry 4286 (class 0 OID 24757)
-- Dependencies: 225
-- Data for Name: questionnaire_influencers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_influencers (id, "firstName", "lastName", "interestedInDonating", range_of_compensation, "user", created_at, updated_at, social_auth_token) FROM stdin;
\.


--
-- TOC entry 4306 (class 0 OID 25365)
-- Dependencies: 245
-- Data for Name: questionnaire_influencers_type_of_contents__type_of_contents_qu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_influencers_type_of_contents__type_of_contents_qu (id, "questionnaire-influencer_id", "type-of-content_id") FROM stdin;
\.


--
-- TOC entry 4310 (class 0 OID 25381)
-- Dependencies: 249
-- Data for Name: questionnaire_influencers_type_of_non_profit_organisations__typ; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_influencers_type_of_non_profit_organisations__typ (id, "questionnaire-influencer_id", "type-of-non-profit-organisation_id") FROM stdin;
\.


--
-- TOC entry 4284 (class 0 OID 24744)
-- Dependencies: 223
-- Data for Name: questionnaire_non_profits; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questionnaire_non_profits (id, "firstName", "lastName", "nameOfOrganization", "user", created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4282 (class 0 OID 24734)
-- Dependencies: 221
-- Data for Name: range_of_compensations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.range_of_compensations (id, range, created_at, updated_at, name) FROM stdin;
1	$0 - $1,000	2020-07-06 20:45:20.621+00	2020-07-22 13:39:25.453+00	$0 - $1,000
2	$1,001 - $5,000	2020-07-06 20:45:25.889+00	2020-07-22 13:39:37.134+00	$1,001 - $5,000
3	$5,001 - $10,000	2020-07-06 20:45:34.252+00	2020-07-22 13:39:47.028+00	$5,001 - $10,000
4	$10,001 - $50,000	2020-07-06 20:45:41.56+00	2020-07-22 13:39:57.208+00	$10,001 - $50,000
5	$50,001 - $100,000	2020-07-06 20:45:48.047+00	2020-07-22 13:40:10.937+00	$50,001 - $100,000
6	$100,001 - $500,000	2020-07-06 20:45:55.402+00	2020-07-22 13:40:27.066+00	$100,001 - $500,000
7	$500,001 - $1,000,000	2020-07-06 20:46:01.631+00	2020-07-22 13:40:41.831+00	$500,001 - $1,000,000
8	$1,000,000+	2020-07-06 20:46:13.102+00	2020-07-22 13:40:50.087+00	$1,000,000+
\.


--
-- TOC entry 4293 (class 0 OID 24803)
-- Dependencies: 232
-- Data for Name: social_auth_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.social_auth_tokens (id, google, amazon, created_at, updated_at, questionnaire_influencer) FROM stdin;
\.


--
-- TOC entry 4299 (class 0 OID 24836)
-- Dependencies: 238
-- Data for Name: sponsor_donations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsor_donations (id, questionnaire_corporate, "promptOnPlatform", "totalAmount", "howShouldItLook", "callToAction", "extraToKnow", created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4316 (class 0 OID 25412)
-- Dependencies: 255
-- Data for Name: sponsors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsors (id, "firstName", "lastName", organisation, "user", created_at, updated_at) FROM stdin;
2	test	sponsor	test sponsor corporation	22	2020-07-22 19:47:26.588+00	2020-07-22 19:47:26.64+00
\.


--
-- TOC entry 4320 (class 0 OID 25454)
-- Dependencies: 259
-- Data for Name: sponsors_type_of_influencers__type_of_influencers_sponsors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsors_type_of_influencers__type_of_influencers_sponsors (id, sponsor_id, "type-of-influencer_id") FROM stdin;
7	2	1
8	2	2
9	2	3
10	2	20
11	2	19
12	2	17
\.


--
-- TOC entry 4340 (class 0 OID 26244)
-- Dependencies: 279
-- Data for Name: sponsors_type_of_non_profit_organisations__type_of_non_profit_o; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsors_type_of_non_profit_organisations__type_of_non_profit_o (id, sponsor_id, "type-of-non-profit-organisation_id") FROM stdin;
7	2	1
8	2	2
9	2	3
10	2	11
11	2	10
12	2	9
\.


--
-- TOC entry 4274 (class 0 OID 24651)
-- Dependencies: 213
-- Data for Name: strapi_administrator; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.strapi_administrator (id, username, email, password, "resetPasswordToken", blocked) FROM stdin;
1	admin	ceefour666@gmail.com	$2a$10$/16X/9PpHd0kMQ5zT/Ulbeqw27r.lC1K/zvBW/7NQSy1gF7DBgCRG	\N	\N
\.


--
-- TOC entry 4270 (class 0 OID 24638)
-- Dependencies: 209
-- Data for Name: strapi_webhooks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.strapi_webhooks (id, name, url, headers, events, enabled) FROM stdin;
\.


--
-- TOC entry 4297 (class 0 OID 24826)
-- Dependencies: 236
-- Data for Name: target_campaigns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.target_campaigns (id, target, created_at, updated_at, proposal) FROM stdin;
\.


--
-- TOC entry 4280 (class 0 OID 24724)
-- Dependencies: 219
-- Data for Name: type_of_contents; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.type_of_contents (id, name, created_at, updated_at, questionnaire_non_profit, questionnaire_influencer, questionnaire_corporate) FROM stdin;
7	Food & Beverage	2020-07-06 20:43:26.142+00	2020-07-06 20:43:26.142+00	\N	\N	\N
11	Music	2020-07-06 20:43:53.133+00	2020-07-06 20:43:53.133+00	\N	\N	\N
14	People & Blogs	2020-07-06 20:44:11.81+00	2020-07-06 20:44:11.81+00	\N	\N	\N
5	Fashion	2020-07-06 20:43:08.923+00	2020-07-11 06:36:15.889+00	\N	10	\N
10	How To	2020-07-06 20:43:46.613+00	2020-07-06 21:16:10.477+00	\N	\N	\N
6	Film & Animation	2020-07-06 20:43:19.024+00	2020-07-14 02:35:32.225+00	\N	14	\N
1	Auto & Vehicles	2020-07-06 20:42:31.004+00	2020-07-14 02:43:52.213+00	\N	15	\N
19	Travel & Events	2020-07-06 20:44:45.159+00	2020-07-17 13:01:25.234+00	\N	10	\N
3	Education	2020-07-06 20:43:01.475+00	2020-07-17 13:01:25.234+00	\N	\N	\N
9	Health & Fitness	2020-07-06 20:43:38.661+00	2020-07-17 13:01:25.234+00	\N	11	\N
2	Comedy	2020-07-06 20:42:56.582+00	2020-07-17 13:01:25.241+00	\N	\N	\N
17	Self Care & Beauty	2020-07-06 20:44:32.87+00	2020-07-17 13:01:25.241+00	\N	12	\N
8	Gaming	2020-07-06 20:43:32.462+00	2020-07-17 13:01:25.241+00	\N	13	\N
13	Nonprofit & Activism	2020-07-06 20:44:05.474+00	2020-07-11 06:33:10.297+00	\N	\N	\N
15	Pets & Animals	2020-07-06 20:44:18.369+00	2020-07-11 06:33:10.306+00	\N	\N	\N
16	Science & Technology	2020-07-06 20:44:25.41+00	2020-07-11 06:33:10.333+00	\N	\N	\N
12	News & Politics	2020-07-06 20:44:00.314+00	2020-07-11 06:33:17.01+00	\N	\N	\N
18	Sports	2020-07-06 20:44:38.42+00	2020-07-11 06:33:17.01+00	\N	\N	\N
4	Entertainment	2020-07-06 20:43:05.69+00	2020-07-11 06:33:17.01+00	\N	\N	\N
\.


--
-- TOC entry 4318 (class 0 OID 25427)
-- Dependencies: 257
-- Data for Name: type_of_influencers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.type_of_influencers (id, name, created_at, updated_at) FROM stdin;
1	Auto & Vehicles	2020-07-22 13:41:26.386+00	2020-07-22 15:41:20.483+00
2	Comedy	2020-07-22 13:41:37.817+00	2020-07-22 15:41:32.011+00
3	Education	2020-07-22 13:41:49.135+00	2020-07-22 15:41:42.998+00
4	Entertainment	2020-07-22 13:41:56.026+00	2020-07-22 15:42:08.108+00
5	Fashion	2020-07-22 13:42:03.374+00	2020-07-22 15:42:21.301+00
6	Film & Animation	2020-07-22 13:42:12.241+00	2020-07-22 15:42:31.05+00
7	Food & Beverage	2020-07-22 13:42:18.837+00	2020-07-22 15:42:47.521+00
8	Gaming	2020-07-22 13:42:36.356+00	2020-07-22 15:42:56.185+00
9	Health & Fitness	2020-07-22 13:42:46.036+00	2020-07-22 15:43:06.866+00
10	How To	2020-07-22 13:43:00.701+00	2020-07-22 15:43:20.256+00
11	Music	2020-07-22 13:43:14.44+00	2020-07-22 15:43:40.626+00
12	News & Politics	2020-07-22 15:43:51.623+00	2020-07-22 15:43:51.623+00
13	Nonprofit & Activism	2020-07-22 15:44:06.051+00	2020-07-22 15:44:06.051+00
14	People & Blogs	2020-07-22 15:44:22.467+00	2020-07-22 15:44:22.467+00
15	Pets & Animals	2020-07-22 15:44:39.404+00	2020-07-22 15:44:39.404+00
16	Science & Technology	2020-07-22 15:44:50.675+00	2020-07-22 15:44:50.675+00
17	Self Care & Beauty	2020-07-22 15:45:05.619+00	2020-07-22 15:45:05.619+00
18	Sports	2020-07-22 15:45:16.516+00	2020-07-22 15:45:16.516+00
19	Sports	2020-07-22 15:45:20.211+00	2020-07-22 15:45:20.211+00
20	Travel & Events	2020-07-22 15:45:28.905+00	2020-07-22 15:45:28.905+00
\.


--
-- TOC entry 4332 (class 0 OID 26198)
-- Dependencies: 271
-- Data for Name: type_of_infs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.type_of_infs (id, name, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4278 (class 0 OID 24714)
-- Dependencies: 217
-- Data for Name: type_of_non_profit_organisations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.type_of_non_profit_organisations (id, name, created_at, updated_at, questionnaire_non_profit, questionnaire_influencer, questionnaire_corporate) FROM stdin;
1	Animals	2020-07-06 20:39:58.339+00	2020-07-22 15:37:40.204+00	\N	13	\N
2	Arts, Culture, Humanities	2020-07-06 20:40:10.733+00	2020-07-22 15:37:54.713+00	\N	\N	\N
3	Community Development	2020-07-06 20:40:24.103+00	2020-07-22 15:38:04.043+00	\N	15	\N
4	Education	2020-07-06 20:40:33.112+00	2020-07-22 15:38:16.257+00	\N	10	\N
5	Environment	2020-07-06 20:40:45.685+00	2020-07-22 15:38:29.068+00	\N	\N	\N
6	Health	2020-07-06 20:40:56.316+00	2020-07-22 15:38:42.765+00	\N	13	\N
7	Human & Civil Rights	2020-07-06 20:41:07.362+00	2020-07-22 15:38:53.342+00	\N	10	\N
8	Human Services	2020-07-06 20:41:17.983+00	2020-07-22 15:39:02.741+00	\N	\N	\N
9	International	2020-07-06 20:41:39.873+00	2020-07-22 15:39:34.4+00	\N	\N	\N
10	Religion	2020-07-06 20:41:51.455+00	2020-07-22 15:39:50.69+00	\N	\N	\N
11	Research & Public Policy	2020-07-06 20:41:59.632+00	2020-07-22 15:40:03.358+00	\N	\N	\N
\.


--
-- TOC entry 4331 (class 0 OID 26189)
-- Dependencies: 270
-- Data for Name: type_of_npos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.type_of_npos (id, name, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4269 (class 0 OID 24630)
-- Dependencies: 208
-- Data for Name: upload_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.upload_file (id, name, "alternativeText", caption, width, height, formats, hash, ext, mime, size, url, "previewUrl", provider, provider_metadata, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 4276 (class 0 OID 24702)
-- Dependencies: 215
-- Data for Name: upload_file_morph; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.upload_file_morph (id, upload_file_id, related_id, related_type, field, "order") FROM stdin;
\.


--
-- TOC entry 4290 (class 0 OID 24783)
-- Dependencies: 229
-- Data for Name: user_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_types (id, type, created_at, updated_at, name) FROM stdin;
1	Influencer	2020-07-06 20:39:00.068+00	2020-07-22 13:49:23.909+00	Influencer
2	Non Profit	2020-07-06 20:39:09.113+00	2020-07-22 13:49:32.402+00	Non-Profit
3	corporation	2020-07-06 20:39:17.672+00	2020-07-22 13:49:38.456+00	Sponsor
\.


--
-- TOC entry 4260 (class 0 OID 24600)
-- Dependencies: 199
-- Data for Name: users-permissions_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."users-permissions_permission" (id, type, controller, action, enabled, policy, role) FROM stdin;
406	application	payment-status	findone	f		2
416	application	payment	find	f		2
395	application	campaign	update	t		1
385	application	campaign	count	t		1
427	application	public-donation	find	t		1
25	content-manager	components	findcomponent	f		1
26	content-manager	components	findcomponent	f		2
27	content-manager	components	listcomponents	f		1
28	content-manager	components	listcomponents	f		2
29	content-manager	components	updatecomponent	f		1
30	content-manager	components	updatecomponent	f		2
31	content-manager	contentmanager	checkuidavailability	f		1
32	content-manager	contentmanager	checkuidavailability	f		2
33	content-manager	contentmanager	count	f		1
35	content-manager	contentmanager	create	f		1
36	content-manager	contentmanager	create	f		2
37	content-manager	contentmanager	delete	f		1
38	content-manager	contentmanager	delete	f		2
39	content-manager	contentmanager	deletemany	f		1
34	content-manager	contentmanager	count	f		2
40	content-manager	contentmanager	deletemany	f		2
42	content-manager	contentmanager	find	f		2
41	content-manager	contentmanager	find	f		1
44	content-manager	contentmanager	findone	f		2
45	content-manager	contentmanager	generateuid	f		1
46	content-manager	contentmanager	generateuid	f		2
47	content-manager	contentmanager	update	f		1
48	content-manager	contentmanager	update	f		2
43	content-manager	contentmanager	findone	f		1
49	content-manager	contenttypes	findcontenttype	f		1
50	content-manager	contenttypes	findcontenttype	f		2
52	content-manager	contenttypes	listcontenttypes	f		2
53	content-manager	contenttypes	updatecontenttype	f		1
54	content-manager	contenttypes	updatecontenttype	f		2
55	content-type-builder	builder	getreservednames	f		1
56	content-type-builder	builder	getreservednames	f		2
51	content-manager	contenttypes	listcontenttypes	f		1
57	content-type-builder	componentcategories	deletecategory	f		1
58	content-type-builder	componentcategories	deletecategory	f		2
59	content-type-builder	componentcategories	editcategory	f		1
60	content-type-builder	componentcategories	editcategory	f		2
61	content-type-builder	components	createcomponent	f		1
62	content-type-builder	components	createcomponent	f		2
63	content-type-builder	components	deletecomponent	f		1
64	content-type-builder	components	deletecomponent	f		2
66	content-type-builder	components	getcomponent	f		2
65	content-type-builder	components	getcomponent	f		1
68	content-type-builder	components	getcomponents	f		2
69	content-type-builder	components	updatecomponent	f		1
67	content-type-builder	components	getcomponents	f		1
70	content-type-builder	components	updatecomponent	f		2
71	content-type-builder	connections	getconnections	f		1
72	content-type-builder	connections	getconnections	f		2
73	content-type-builder	contenttypes	createcontenttype	f		1
75	content-type-builder	contenttypes	deletecontenttype	f		1
74	content-type-builder	contenttypes	createcontenttype	f		2
77	content-type-builder	contenttypes	getcontenttype	f		1
78	content-type-builder	contenttypes	getcontenttype	f		2
79	content-type-builder	contenttypes	getcontenttypes	f		1
76	content-type-builder	contenttypes	deletecontenttype	f		2
80	content-type-builder	contenttypes	getcontenttypes	f		2
247	application	user-type	find	t		1
81	content-type-builder	contenttypes	updatecontenttype	f		1
83	email	email	send	f		1
84	email	email	send	f		2
82	content-type-builder	contenttypes	updatecontenttype	f		2
86	upload	proxy	uploadproxy	f		2
195	application	range-of-compensation	create	f		1
169	application	type-of-non-profit-organisation	count	t		1
87	upload	upload	count	f		1
97	upload	upload	search	f		1
104	users-permissions	auth	callback	t		2
124	users-permissions	user	destroyall	f		2
134	users-permissions	userspermissions	createrole	f		2
143	users-permissions	userspermissions	getpermissions	f		1
152	users-permissions	userspermissions	getroles	f		2
162	users-permissions	userspermissions	updateadvancedsettings	f		2
170	application	type-of-non-profit-organisation	count	f		2
179	application	type-of-non-profit-organisation	update	f		1
196	application	range-of-compensation	create	f		2
204	application	range-of-compensation	update	f		2
396	application	campaign	update	f		2
114	users-permissions	auth	resetpassword	t		2
386	application	campaign	count	t		2
248	application	user-type	find	f		2
373	application	type-of-influencer	count	t		1
415	application	payment	find	t		1
355	application	proposal	find	t		1
275	application	proposal-status	update	f		1
425	application	public-donation	delete	t		1
405	application	payment-status	findone	t		1
319	application	influencer	find	t		1
265	application	proposal-status	count	t		1
329	application	non-profit	delete	f		1
339	application	proposal-visibility	create	f		1
348	application	proposal-visibility	update	f		2
364	application	sponsor	create	f		2
383	application	type-of-influencer	update	f		1
88	upload	upload	count	f		2
96	upload	upload	getsettings	f		2
105	users-permissions	auth	connect	t		1
115	users-permissions	auth	sendemailconfirmation	f		1
136	users-permissions	userspermissions	deleteprovider	f		2
144	users-permissions	userspermissions	getpermissions	f		2
153	users-permissions	userspermissions	getroutes	f		1
163	users-permissions	userspermissions	updateemailtemplate	f		1
171	application	type-of-non-profit-organisation	create	f		1
356	application	proposal	find	t		2
197	application	range-of-compensation	delete	f		1
398	application	payment-status	count	f		2
407	application	payment-status	update	f		1
426	application	public-donation	delete	f		2
417	application	payment	findone	t		1
387	application	campaign	create	t		1
125	users-permissions	user	find	t		1
249	application	user-type	findone	t		1
369	application	sponsor	findone	t		1
266	application	proposal-status	count	f		2
276	application	proposal-status	update	f		2
320	application	influencer	find	f		2
330	application	non-profit	delete	f		2
341	application	proposal-visibility	delete	f		1
347	application	proposal-visibility	update	f		1
382	application	type-of-influencer	findone	f		2
85	upload	proxy	uploadproxy	f		1
98	upload	upload	search	f		2
108	users-permissions	auth	emailconfirmation	t		2
118	users-permissions	user	count	f		2
128	users-permissions	user	findone	f		2
135	users-permissions	userspermissions	deleteprovider	f		1
145	users-permissions	userspermissions	getpolicies	f		1
154	users-permissions	userspermissions	getroutes	f		2
165	users-permissions	userspermissions	updateproviders	f		1
172	application	type-of-non-profit-organisation	create	f		2
388	application	campaign	create	f		2
350	application	proposal	count	t		2
198	application	range-of-compensation	delete	f		2
412	application	payment	create	f		2
432	application	public-donation	update	f		2
250	application	user-type	findone	f		2
423	application	public-donation	create	t		1
371	application	sponsor	update	t		1
267	application	proposal-status	create	f		1
397	application	payment-status	count	t		1
361	application	sponsor	count	t		1
321	application	influencer	findone	t		1
332	application	non-profit	find	f		2
340	application	proposal-visibility	create	f		2
380	application	type-of-influencer	find	f		2
89	upload	upload	destroy	f		1
99	upload	upload	updatesettings	f		1
109	users-permissions	auth	forgotpassword	f		1
119	users-permissions	user	create	f		1
129	users-permissions	user	me	t		1
139	users-permissions	userspermissions	getadvancedsettings	f		1
149	users-permissions	userspermissions	getrole	f		1
159	users-permissions	userspermissions	searchusers	f		1
168	users-permissions	userspermissions	updaterole	f		2
173	application	type-of-non-profit-organisation	delete	f		1
389	application	campaign	delete	f		1
404	application	payment-status	find	f		2
414	application	payment	delete	f		2
331	application	non-profit	find	t		1
351	application	proposal	create	t		1
358	application	proposal	findone	t		2
199	application	range-of-compensation	find	t		1
424	application	public-donation	create	t		2
268	application	proposal-status	create	f		2
322	application	influencer	findone	f		2
342	application	proposal-visibility	delete	f		2
366	application	sponsor	delete	f		2
377	application	type-of-influencer	delete	f		1
90	upload	upload	destroy	f		2
100	upload	upload	updatesettings	f		2
110	users-permissions	auth	forgotpassword	t		2
130	users-permissions	user	me	t		2
140	users-permissions	userspermissions	getadvancedsettings	f		2
150	users-permissions	userspermissions	getrole	f		2
160	users-permissions	userspermissions	searchusers	f		2
174	application	type-of-non-profit-organisation	delete	f		2
390	application	campaign	delete	f		2
200	application	range-of-compensation	find	f		2
402	application	payment-status	delete	f		2
410	application	payment	count	f		2
241	application	user-type	count	f		2
120	users-permissions	user	create	t		2
430	application	public-donation	findone	t		2
269	application	proposal-status	delete	f		1
333	application	non-profit	findone	t		1
419	application	payment	update	t		1
343	application	proposal-visibility	find	t		1
363	application	sponsor	create	t		1
323	application	influencer	update	t		1
354	application	proposal	delete	f		2
374	application	type-of-influencer	count	f		2
384	application	type-of-influencer	update	f		2
313	application	influencer	count	t		1
91	upload	upload	find	f		1
103	users-permissions	auth	callback	f		1
113	users-permissions	auth	resetpassword	f		1
123	users-permissions	user	destroyall	f		1
132	users-permissions	user	update	f		2
141	users-permissions	userspermissions	getemailtemplate	f		1
151	users-permissions	userspermissions	getroles	f		1
161	users-permissions	userspermissions	updateadvancedsettings	f		1
428	application	public-donation	find	t		2
401	application	payment-status	delete	f		1
418	application	payment	findone	f		2
242	application	user-type	create	f		1
201	application	range-of-compensation	findone	t		1
175	application	type-of-non-profit-organisation	find	t		1
314	application	influencer	count	f		2
379	application	type-of-influencer	find	t		1
270	application	proposal-status	delete	f		2
327	application	non-profit	create	t		1
337	application	proposal-visibility	count	t		1
409	application	payment	count	t		1
391	application	campaign	find	t		1
352	application	proposal	create	f		2
362	application	sponsor	count	f		2
370	application	sponsor	findone	f		2
92	upload	upload	find	f		2
101	upload	upload	upload	f		1
111	users-permissions	auth	register	f		1
121	users-permissions	user	destroy	f		1
142	users-permissions	userspermissions	getemailtemplate	f		2
155	users-permissions	userspermissions	index	f		1
164	users-permissions	userspermissions	updateemailtemplate	f		2
176	application	type-of-non-profit-organisation	find	f		2
180	application	type-of-non-profit-organisation	update	f		2
202	application	range-of-compensation	findone	f		2
392	application	campaign	find	t		2
413	application	payment	delete	f		1
325	application	non-profit	count	t		1
271	application	proposal-status	find	t		1
251	application	user-type	update	f		1
131	users-permissions	user	update	t		1
403	application	payment-status	find	t		1
243	application	user-type	count	t		1
315	application	influencer	create	t		1
422	application	public-donation	count	t		2
334	application	non-profit	findone	f		2
344	application	proposal-visibility	find	f		2
353	application	proposal	delete	f		1
365	application	sponsor	delete	f		1
378	application	type-of-influencer	delete	f		2
93	upload	upload	findone	f		1
102	upload	upload	upload	f		2
112	users-permissions	auth	register	t		2
122	users-permissions	user	destroy	f		2
133	users-permissions	userspermissions	createrole	f		1
146	users-permissions	userspermissions	getpolicies	f		2
156	users-permissions	userspermissions	index	f		2
166	users-permissions	userspermissions	updateproviders	f		2
399	application	payment-status	create	f		1
408	application	payment-status	update	f		2
420	application	payment	update	f		2
244	application	user-type	create	f		2
335	application	non-profit	update	t		1
177	application	type-of-non-profit-organisation	findone	t		1
345	application	proposal-visibility	findone	t		1
316	application	influencer	create	f		2
393	application	campaign	findone	t		1
272	application	proposal-status	find	f		2
357	application	proposal	findone	t		1
429	application	public-donation	findone	t		1
367	application	sponsor	find	t		1
324	application	influencer	update	f		2
376	application	type-of-influencer	create	f		2
94	upload	upload	findone	f		2
107	users-permissions	auth	emailconfirmation	f		1
116	users-permissions	auth	sendemailconfirmation	f		2
138	users-permissions	userspermissions	deleterole	f		2
147	users-permissions	userspermissions	getproviders	f		1
157	users-permissions	userspermissions	init	t		1
178	application	type-of-non-profit-organisation	findone	f		2
203	application	range-of-compensation	update	f		1
400	application	payment-status	create	f		2
273	application	proposal-status	findone	t		1
394	application	campaign	findone	t		2
245	application	user-type	delete	f		1
252	application	user-type	update	f		2
126	users-permissions	user	find	t		2
193	application	range-of-compensation	count	t		1
381	application	type-of-influencer	findone	t		1
411	application	payment	create	t		1
421	application	public-donation	count	t		1
431	application	public-donation	update	t		1
317	application	influencer	delete	f		1
326	application	non-profit	count	f		2
336	application	non-profit	update	f		2
346	application	proposal-visibility	findone	f		2
360	application	proposal	update	f		2
372	application	sponsor	update	f		2
95	upload	upload	getsettings	f		1
106	users-permissions	auth	connect	t		2
117	users-permissions	user	count	t		1
137	users-permissions	userspermissions	deleterole	f		1
148	users-permissions	userspermissions	getproviders	f		2
158	users-permissions	userspermissions	init	t		2
167	users-permissions	userspermissions	updaterole	f		1
349	application	proposal	count	t		1
194	application	range-of-compensation	count	f		2
359	application	proposal	update	t		1
318	application	influencer	delete	f		2
246	application	user-type	delete	f		2
127	users-permissions	user	findone	t		1
274	application	proposal-status	findone	f		2
328	application	non-profit	create	f		2
338	application	proposal-visibility	count	f		2
368	application	sponsor	find	f		2
375	application	type-of-influencer	create	f		1
\.


--
-- TOC entry 4273 (class 0 OID 24648)
-- Dependencies: 212
-- Data for Name: users-permissions_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."users-permissions_role" (id, name, description, type) FROM stdin;
1	Authenticated	Default role given to authenticated user.	authenticated
2	Public	Default role given to unauthenticated user.	public
\.


--
-- TOC entry 4258 (class 0 OID 24589)
-- Dependencies: 197
-- Data for Name: users-permissions_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."users-permissions_user" (id, username, email, provider, password, "resetPasswordToken", confirmed, blocked, role, created_at, updated_at, "hasSubmitQuestionnaire", user_type, social_auth_token, influencer, non_profit, sponsor) FROM stdin;
22	test@sponsor.com	test@sponsor.com	local	$2a$10$aaX2lxNMFuQi4CyzK56Wx.J10aRENOTlUlugC6atKx7kffJQaQh6y	\N	t	\N	1	2020-07-22 19:46:57.303+00	2020-07-22 19:47:26.716+00	t	3	\N	\N	\N	2
23	test@influencer.com	test@influencer.com	local	$2a$10$1OCK8oxv/WrpMLH4dPvav.RISpg.blqP3gvp043XCwUdZRbKP00zC	\N	t	\N	1	2020-07-22 19:47:49.223+00	2020-07-22 19:48:27.967+00	t	1	\N	9	\N	\N
24	test@nonprofit.com	test@nonprofit.com	local	$2a$10$cWEDgy1/hUet6y4p3JOR2u9hg1eSDjM2XEbD3UdBMPrn57AmgSGEC	\N	t	\N	1	2020-07-22 19:49:43.503+00	2020-07-22 19:50:21.01+00	t	2	\N	\N	3	\N
\.


--
-- TOC entry 4350 (class 0 OID 0)
-- Dependencies: 260
-- Name: campaigns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.campaigns_id_seq', 1, true);


--
-- TOC entry 4351 (class 0 OID 0)
-- Dependencies: 204
-- Name: core_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.core_store_id_seq', 77, true);


--
-- TOC entry 4352 (class 0 OID 0)
-- Dependencies: 200
-- Name: influencers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.influencers_id_seq', 9, true);


--
-- TOC entry 4353 (class 0 OID 0)
-- Dependencies: 274
-- Name: influencers_type_of_influencers__type_of_influencers_inf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.influencers_type_of_influencers__type_of_influencers_inf_id_seq', 31, true);


--
-- TOC entry 4354 (class 0 OID 0)
-- Dependencies: 280
-- Name: influencers_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.influencers_type_of_non_profit_organisations__type_of_no_id_seq', 30, true);


--
-- TOC entry 4355 (class 0 OID 0)
-- Dependencies: 250
-- Name: non_profits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.non_profits_id_seq', 3, true);


--
-- TOC entry 4356 (class 0 OID 0)
-- Dependencies: 276
-- Name: non_profits_type_of_influencers__type_of_influencers_non_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.non_profits_type_of_influencers__type_of_influencers_non_id_seq', 18, true);


--
-- TOC entry 4357 (class 0 OID 0)
-- Dependencies: 282
-- Name: non_profits_type_of_non_profit_organisations__type_of_no_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.non_profits_type_of_non_profit_organisations__type_of_no_id_seq', 18, true);


--
-- TOC entry 4358 (class 0 OID 0)
-- Dependencies: 241
-- Name: organizationlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.organizationlist_id_seq', 192, true);


--
-- TOC entry 4359 (class 0 OID 0)
-- Dependencies: 203
-- Name: organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.organizations_id_seq', 1, false);


--
-- TOC entry 4360 (class 0 OID 0)
-- Dependencies: 262
-- Name: payment_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payment_statuses_id_seq', 3, true);


--
-- TOC entry 4361 (class 0 OID 0)
-- Dependencies: 266
-- Name: payments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payments_id_seq', 3, true);


--
-- TOC entry 4362 (class 0 OID 0)
-- Dependencies: 233
-- Name: proposal_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proposal_statuses_id_seq', 7, true);


--
-- TOC entry 4363 (class 0 OID 0)
-- Dependencies: 252
-- Name: proposal_visibilities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proposal_visibilities_id_seq', 5, true);


--
-- TOC entry 4364 (class 0 OID 0)
-- Dependencies: 239
-- Name: proposals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proposals_id_seq', 1, true);


--
-- TOC entry 4365 (class 0 OID 0)
-- Dependencies: 272
-- Name: proposals_proposal_visibilities__proposal_visibilities_p_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proposals_proposal_visibilities__proposal_visibilities_p_id_seq', 2, true);


--
-- TOC entry 4366 (class 0 OID 0)
-- Dependencies: 264
-- Name: public_donations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.public_donations_id_seq', 1, false);


--
-- TOC entry 4367 (class 0 OID 0)
-- Dependencies: 226
-- Name: questionnaire_corporates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_corporates_id_seq', 1, true);


--
-- TOC entry 4368 (class 0 OID 0)
-- Dependencies: 242
-- Name: questionnaire_corporates_type_of_contents__type_of_conte_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_corporates_type_of_contents__type_of_conte_id_seq', 1, false);


--
-- TOC entry 4369 (class 0 OID 0)
-- Dependencies: 246
-- Name: questionnaire_corporates_type_of_non_profit_organisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_corporates_type_of_non_profit_organisation_id_seq', 1, false);


--
-- TOC entry 4370 (class 0 OID 0)
-- Dependencies: 224
-- Name: questionnaire_influencers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_influencers_id_seq', 16, true);


--
-- TOC entry 4371 (class 0 OID 0)
-- Dependencies: 244
-- Name: questionnaire_influencers_type_of_contents__type_of_cont_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_influencers_type_of_contents__type_of_cont_id_seq', 1, false);


--
-- TOC entry 4372 (class 0 OID 0)
-- Dependencies: 248
-- Name: questionnaire_influencers_type_of_non_profit_organisatio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_influencers_type_of_non_profit_organisatio_id_seq', 1, false);


--
-- TOC entry 4373 (class 0 OID 0)
-- Dependencies: 222
-- Name: questionnaire_non_profits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_non_profits_id_seq', 3, true);


--
-- TOC entry 4374 (class 0 OID 0)
-- Dependencies: 220
-- Name: range_of_compensations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.range_of_compensations_id_seq', 8, true);


--
-- TOC entry 4375 (class 0 OID 0)
-- Dependencies: 231
-- Name: social_auth_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.social_auth_tokens_id_seq', 17, true);


--
-- TOC entry 4376 (class 0 OID 0)
-- Dependencies: 237
-- Name: sponsor_donations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sponsor_donations_id_seq', 1, false);


--
-- TOC entry 4377 (class 0 OID 0)
-- Dependencies: 254
-- Name: sponsors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sponsors_id_seq', 2, true);


--
-- TOC entry 4378 (class 0 OID 0)
-- Dependencies: 258
-- Name: sponsors_type_of_influencers__type_of_influencers_sponso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sponsors_type_of_influencers__type_of_influencers_sponso_id_seq', 12, true);


--
-- TOC entry 4379 (class 0 OID 0)
-- Dependencies: 278
-- Name: sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sponsors_type_of_non_profit_organisations__type_of_non_p_id_seq', 12, true);


--
-- TOC entry 4380 (class 0 OID 0)
-- Dependencies: 206
-- Name: strapi_administrator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.strapi_administrator_id_seq', 1, true);


--
-- TOC entry 4381 (class 0 OID 0)
-- Dependencies: 202
-- Name: strapi_webhooks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.strapi_webhooks_id_seq', 1, false);


--
-- TOC entry 4382 (class 0 OID 0)
-- Dependencies: 235
-- Name: target_campaigns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.target_campaigns_id_seq', 1, false);


--
-- TOC entry 4383 (class 0 OID 0)
-- Dependencies: 218
-- Name: type_of_contents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_of_contents_id_seq', 19, true);


--
-- TOC entry 4384 (class 0 OID 0)
-- Dependencies: 256
-- Name: type_of_influencers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_of_influencers_id_seq', 20, true);


--
-- TOC entry 4385 (class 0 OID 0)
-- Dependencies: 268
-- Name: type_of_infs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_of_infs_id_seq', 1, false);


--
-- TOC entry 4386 (class 0 OID 0)
-- Dependencies: 216
-- Name: type_of_non_profit_organisations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_of_non_profit_organisations_id_seq', 20, true);


--
-- TOC entry 4387 (class 0 OID 0)
-- Dependencies: 269
-- Name: type_of_npos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_of_npos_id_seq', 1, false);


--
-- TOC entry 4388 (class 0 OID 0)
-- Dependencies: 201
-- Name: upload_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.upload_file_id_seq', 1, false);


--
-- TOC entry 4389 (class 0 OID 0)
-- Dependencies: 214
-- Name: upload_file_morph_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.upload_file_morph_id_seq', 1, false);


--
-- TOC entry 4390 (class 0 OID 0)
-- Dependencies: 228
-- Name: user_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_types_id_seq', 3, true);


--
-- TOC entry 4391 (class 0 OID 0)
-- Dependencies: 198
-- Name: users-permissions_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."users-permissions_permission_id_seq"', 432, true);


--
-- TOC entry 4392 (class 0 OID 0)
-- Dependencies: 205
-- Name: users-permissions_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."users-permissions_role_id_seq"', 2, true);


--
-- TOC entry 4393 (class 0 OID 0)
-- Dependencies: 196
-- Name: users-permissions_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."users-permissions_user_id_seq"', 24, true);


-- Completed on 2020-07-22 20:06:02 UTC

--
-- PostgreSQL database dump complete
--

